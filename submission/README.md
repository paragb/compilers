Readme:-
1) Scoping rules are implemented for functions, global variables are also allowed. while, scoping rules of blocks are not taken care of.
2) variable declarations of type "VAR a, b, type" are allowed.
3) Arrays and pointers are handled. while, structs and multi-dimensional arrays are not taken care of.
4) Constant declarations are allowed.
5) Assignment statements of form a, b ,v = 1,2 ,3; are allowed. Only if, number of elements on both side of equal are same.
6) fmt.Println and fmt.Readln are parsed and IR is generated appropriately.
7) Multiple functions are allowed, and functions can have multiple arguments.
8) Type Conversion is not handled.
9) Goto and labeled statements are handled.
10) For and If statements are handled.
11) Backpatching is done for generating final IR code.
12) Increment by 1 and Decrement by 1 are handled.
13) Return statement is handled
