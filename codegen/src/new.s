	.section .data

	integerFormat:
.ascii "%d\0"


A7:
.long 0
A7_end:

A6:
.long 0
A6_end:

A1:
.long 0
A1_end:

A0:
.long 0
A0_end:

A3:
.long 0
A3_end:

A2:
.long 0
A2_end:

A5:
.long 0
A5_end:

A4:
.long 0
A4_end:

D8:
.long 0
D8_end:

D9:
.long 0
D9_end:

D6:
.long 0
D6_end:

D7:
.long 0
D7_end:

D5:
.long 0
D5_end:

E1:
.long 0
E1_end:

E0:
.long 0
E0_end:

D0:
.long 0
D0_end:

C:
.long 0
C_end:

E:
.long 0
E_end:

D2:
.long 0
D2_end:

D_1:
.long 0
D_1_end:

D10:
.long 0
D10_end:

	.section .text

	
	.globl _start

_start:

	movl	$10,	C

	movl	$1,	A0

	movl	$1,	A1

	movl	$1,	A2

	movl	$2,	A3

	movl	$0,	A4

	movl	$1,	A5

	movl	$0,	A6

	movl	$1,	A7

	movl	C,	%ecx

	cmpl	$0,	%ecx

	jle	B0

	movl	A1,	%edx

	addl	A0,	%edx

	movl	%edx,	D7

	movl	A2,	%eax

	addl	D7,	%eax

	movl	%eax,	D8

	movl	A3,	%ebx

	addl	D8,	%ebx

	movl	%ebx,	D9

	movl	%eax,	D8

	movl	A4,	%eax

	addl	D9,	%eax

	movl	%eax,	D0

	movl	%ebx,	D9

	movl	A5,	%ebx

	addl	D0,	%ebx

	movl	%ebx,	D_1

	movl	%eax,	D0

	movl	A6,	%eax

	addl	D_1,	%eax

	movl	%eax,	D2

	movl	%ebx,	D_1

	movl	A7,	%ebx

	addl	D2,	%ebx

	movl	%ebx,	E0

	movl	%ebx,	E

	jmp	B1

B0:

	movl	%ebx,	E0

	movl	A1,	%ebx

	subl	A0,	%ebx

	movl	%ebx,	D5

	movl	%eax,	D2

	movl	A2,	%eax

	addl	A5,	%eax

	movl	%eax,	D6

	movl	%edx,	D7

	movl	A3,	%edx

	addl	D7,	%edx

	movl	%edx,	D7

	movl	%ecx,	C

	movl	A4,	%ecx

	addl	D8,	%ecx

	movl	%ecx,	D8

	movl	%ecx,	D8

	movl	A5,	%ecx

	addl	D9,	%ecx

	movl	%ecx,	D9

	movl	%ecx,	D9

	movl	A6,	%ecx

	addl	D0,	%ecx

	movl	%ecx,	D10

	movl	%ecx,	D10

	movl	A7,	%ecx

	addl	E0,	%ecx

	movl	%ecx,	E1

	movl	%ecx,	E

B1:

	movl	%ebx,	D5

	movl	%eax,	D6

	movl	%edx,	D7

	movl	%ecx,	E1

	pushl	E

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	$0,	D2

	call exit

