	.section .data

	integerFormat:
.ascii "%d\0"


G1:
.long 0
G1_end:

G3:
.long 0
G3_end:

T:
.long 0
T_end:

G:
.long 0
G_end:

G2:
.long 0
G2_end:

	.section .text

	
	.globl _start

_start:

	movl	$5,	G

	call	myfunc

	movl	T,	%ecx

	movl	%eax,	%ecx

	call exit

myfunc:

	pushl	%ebp

	movl	%esp,	%ebp

	movl	G,	%edx

	cmpl	$0,	%edx

	je	B0

	jmp	B1

B0:

	call exit

B1:

	movl	$5,	G1

	movl	%edx,	G

	movl	%ecx,	T

	pushl	G

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	$4,	G2

	movl	G,	%ecx

	subl	$1,	%ecx

	movl	%ecx,	G3

	movl	%ecx,	G

	call	myfunc

	movl	T,	%edx

	movl	%eax,	%edx

	movl	%ebp,	%esp

	popl	%ebp

	movl	%edx,	%eax

	ret

