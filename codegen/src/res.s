	.section .data

	integerFormat:
.ascii "%d\0"


A:
.long 0
A_end:

B:
.long 0
B_end:

	.section .text

	
	.globl _start

_start:

	call	foo

	movl	B,	%ecx

	movl	%eax,	%ecx

	call exit

foo:

	pushl	%ebp

	movl	%esp,	%ebp

	movl	$2,	A

	movl	%ecx,	B

	pushl	A

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	pushl	$A

	pushl	$integerFormat

	call	scanf

	popl	%eax

	popl	%eax

	pushl	A

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	%ebp,	%esp

	popl	%ebp

	movl	A,	%ecx

	movl	%ecx,	%eax

	ret

