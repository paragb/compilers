	.section .data

	integerFormat:
.ascii "%d\0"


H0:
.long 0
H0_end:

H1:
.long 0
H1_end:

D1749:
.long 0
D1749_end:

D1748:
.long 0
D1748_end:

D1741:
.long 0
D1741_end:

D1740:
.long 0
D1740_end:

D1742:
.long 0
D1742_end:

D1745:
.long 0
D1745_end:

D1747:
.long 0
D1747_end:

D1746:
.long 0
D1746_end:

A1:
.long 0
A1_end:

A0:
.long 0
A0_end:

A3:
.long 0
A3_end:

A2:
.long 0
A2_end:

A5:
.long 0
A5_end:

A4:
.long 0
A4_end:

A7:
.long 0
A7_end:

A6:
.long 0
A6_end:

DUMMY:
.long 0
DUMMY_end:

C:
.long 0
C_end:

D1738:
.long 0
D1738_end:

D1739:
.long 0
D1739_end:

H:
.long 0
H_end:

D1750:
.long 0
D1750_end:

D1737:
.long 0
D1737_end:

	.section .text

	
	.globl _start

_start:

	movl	$0,	DUMMY

	movl	$10,	C

	movl	$1,	A0

	movl	$1,	A1

	movl	$1,	A2

	movl	$2,	A3

	movl	$0,	A4

	movl	$1,	A5

	movl	$0,	A6

	movl	$1,	A7

	movl	C,	%ecx

	cmpl	$0,	%ecx

	jg	B0

	jmp	B2

B0:

	movl	A1,	%edx

	addl	A0,	%edx

	movl	%edx,	D1737

	movl	A2,	%eax

	addl	D1737,	%eax

	movl	%eax,	D1738

	movl	A3,	%ebx

	addl	D1738,	%ebx

	movl	%ebx,	D1739

	movl	%ecx,	C

	movl	A4,	%ecx

	addl	D1739,	%ecx

	movl	%ecx,	D1740

	movl	%ebx,	D1739

	movl	A5,	%ebx

	addl	D1740,	%ebx

	movl	%ebx,	D1741

	movl	%ecx,	D1740

	movl	A6,	%ecx

	addl	D1741,	%ecx

	movl	%ecx,	D1742

	movl	%ebx,	D1741

	movl	A7,	%ebx

	addl	D1742,	%ebx

	movl	%ebx,	H0

	movl	%ebx,	H

	jmp	B3

B2:

B2:

	movl	%ebx,	H0

	movl	A1,	%ebx

	subl	A0,	%ebx

	movl	%ebx,	D1745

	movl	%ecx,	D1742

	movl	A2,	%ecx

	addl	D1745,	%ecx

	movl	%ecx,	D1746

	movl	%eax,	D1738

	movl	A3,	%eax

	addl	D1746,	%eax

	movl	%eax,	D1747

	movl	%edx,	D1737

	movl	A4,	%edx

	addl	D1747,	%edx

	movl	%edx,	D1748

	movl	%eax,	D1747

	movl	A5,	%eax

	addl	D1748,	%eax

	movl	%eax,	D1749

	movl	%edx,	D1748

	movl	A6,	%edx

	addl	D1749,	%edx

	movl	%edx,	D1750

	movl	%eax,	D1749

	movl	A7,	%eax

	addl	D1750,	%eax

	movl	%eax,	H1

B3:

	call exit

