import generator
import code
import symtab
TAC = code.ThreeAddressCode()
symTab = symtab.SymTab(TAC.code)
symTab.GetBasicBlock()
assemblyCode = generator.generator(symTab,TAC)
assemblyCode.printCode()
