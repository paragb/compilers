	.section .data

	integerFormat:
.ascii "%d\0"


I:
.long 0
I_end:

RES:
.long 0
RES_end:

K:
.long 0
K_end:

J:
.long 0
J_end:

	.section .text

	
	.globl _start

_start:

	movl	$0,	I

	movl	$0,	RES

	jmp	B9

B0:

	movl	$0,	J

	jmp	B6

B1:

	movl	$0,	K

	jmp	B3

B2:

	movl	RES,	%ecx

	addl	$1,	%ecx

	movl	%ecx,	RES

	movl	%ecx,	RES

	pushl	RES

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	K,	%ecx

	addl	$1,	%ecx

	movl	%ecx,	K

B3:

	cmpl	$9,	%ecx

	jle	B2

	jmp	B5

B5:

B5:

	movl	J,	%edx

	addl	$1,	%edx

	movl	%edx,	J

	movl	%edx,	J

	movl	%ecx,	K

	pushl	J

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

B6:

	movl	J,	%ecx

	cmpl	$9,	%ecx

	jle	B1

	jmp	B8

B8:

B8:

	movl	I,	%edx

	addl	$1,	%edx

	movl	%edx,	I

	movl	%edx,	I

	movl	%ecx,	J

	pushl	I

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

B9:

	movl	I,	%ecx

	cmpl	$9,	%ecx

	jle	B0

	jmp	B11

B11:

B11:

	movl	%ecx,	I

	pushl	RES

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	call exit

