from generator import *
class SymTab:
    def __init__(self, TAcode):
        self.code = TAcode
        self.SymbolTable={}
        self.special=['jmp','if', 'goto','ret','exit','print','call','read']
        self.operators=['+','-','*','/','=','<','>','<=','>=']
        self.ProgramPointSymbolTable={}
        self.BasicBlockValue={}
        self.LeaderList=[]
        self.BasicMapping={}
        self.functionList=[]

    def MapBasicBlock(self):
    	temp=0
    	for i in self.LeaderList[:-1]:
    		if self.code[i][1]=='func':
    			self.BasicMapping[i]=self.code[i][2]
    		else:
    			self.BasicMapping[i]='B'+str(temp)
    			temp=temp+1

    def GetBasicBlock(self):
        for i in range(0,len(self.code)):
            # print self.code[i]
            if(self.code[i][1] in ['goto','jmp']):
                self.LeaderList.append(int(self.code[i][2]))
                self.LeaderList.append(i+1)
            if self.code[i][1] in ['func']:
                self.LeaderList.append(i)
                self.functionList.append(self.code[i][2])
                ##printself.code[i][2],'t'
        self.LeaderList.append(len(self.code))
        self.LeaderList = sorted(self.LeaderList)
        self.InitializeTable()
        for i in range(0,len(self.LeaderList)-1):
            self.PopulateSymbolTable(i,self.LeaderList[i],self.LeaderList[i+1])
        self.MapBasicBlock()

    def InitializeTable(self):
        for i in range(len(self.code)-1,-1,-1):
            if 'func' not in self.code[i]:
                for word in self.code[i]:
                    try:
                        if int(word) in range(1,1000):
                            pass
                    except:
                        if word not in self.special+self.operators+self.functionList:
                            self.ProgramPointSymbolTable[word]={'Status': 'dead', 'NextUse':len(self.code)}.copy()

    def IntializeTableForBasicBlock(self, StartPoint,EndPoint):
        for key in self.ProgramPointSymbolTable:
            if key[:4]=='temp':
                self.ProgramPointSymbolTable[key]['Status']='dead'
                self.ProgramPointSymbolTable[key]['NextUse']=EndPoint
            else:
                self.ProgramPointSymbolTable[key]['Status']='live'
                self.ProgramPointSymbolTable[key]['NextUse']=EndPoint

    def PopulateSymbolTable(self,BasicBlockNumber,Leader,NextLeader):
        self.SymbolTable[BasicBlockNumber]={}
        self.IntializeTableForBasicBlock(Leader,NextLeader)
        for i in range(NextLeader-1,Leader-1,-1):
            LineNumber=self.code[i][0]
            #assert(LineNumber==i)
            if self.code[i][1] in self.operators:
                oldNextUse = self.ProgramPointSymbolTable[self.code[i][2]]['NextUse']
                self.ProgramPointSymbolTable[self.code[i][2]]={'Status': 'dead', 'NextUse':int(oldNextUse)}.copy()
                for j in [3,4]:
                    try:
                        try:
                            if int(self.code[i][j]) in range(1,1000):
                                pass

                        except:
                            self.ProgramPointSymbolTable[self.code[i][j]]={'Status': 'live', 'NextUse':int(LineNumber)}.copy()
                    except:
                        pass
            if self.code[i][1]=='print':
                self.ProgramPointSymbolTable[self.code[i][2]]={'Status': 'live', 'NextUse':int(LineNumber)}.copy()
            if self.code[i][1]=='read':
                self.ProgramPointSymbolTable[self.code[i][2]]={'Status': 'live', 'NextUse':int(LineNumber)}.copy()

            self.SymbolTable[BasicBlockNumber][i]=self.ProgramPointSymbolTable.copy()
