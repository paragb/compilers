	.section .data

	integerFormat:
.ascii "%d\0"


A:
.long 0
A_end:

C:
.long 0
C_end:

B:
.long 0
B_end:

E:
.long 0
E_end:

D:
.long 0
D_end:

F:
.long 0
F_end:

	.section .text

	
	.globl _start

_start:

	call	func_read

	movl	D,	%ecx

	movl	%eax,	%ecx

	call	func_add

	movl	E,	%edx

	movl	%eax,	%edx

	call	func_sub

	movl	F,	%eax

	movl	%eax,	%eax

	call exit

func_read:

	pushl	%ebp

	movl	%esp,	%ebp

	movl	%eax,	F

	movl	%edx,	E

	movl	%ecx,	D

	pushl	$A

	pushl	$integerFormat

	call	scanf

	popl	%eax

	popl	%eax

	pushl	$B

	pushl	$integerFormat

	call	scanf

	popl	%eax

	popl	%eax

	movl	%ebp,	%esp

	popl	%ebp

	movl	A,	%ecx

	movl	%ecx,	%eax

	ret

func_add:

	pushl	%ebp

	movl	%esp,	%ebp

	movl	B,	%edx

	addl	A,	%edx

	movl	%edx,	C

	movl	%edx,	C

	movl	%ecx,	A

	pushl	C

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	%ebp,	%esp

	popl	%ebp

	movl	A,	%ecx

	movl	%ecx,	%eax

	ret

func_sub:

	pushl	%ebp

	movl	%esp,	%ebp

	movl	B,	%edx

	subl	A,	%edx

	movl	%edx,	C

	movl	%edx,	C

	movl	%ecx,	A

	pushl	C

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	%ebp,	%esp

	popl	%ebp

	movl	A,	%ecx

	movl	%ecx,	%eax

	ret

