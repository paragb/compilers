import code
import re
from symtab import *
def generator(SymTab, ThreeAddressCode):
    SymbolTable=SymTab.SymbolTable
    LeaderList=SymTab.LeaderList
    BasicMapping=SymTab.BasicMapping
    functionList = SymTab.functionList
    assembly=code.x86Code(SymbolTable,ThreeAddressCode,LeaderList,BasicMapping,functionList)
    allVar = [i for i in SymbolTable[0][0].keys()]
    allVar = list(set(allVar))
    # print allVar
    assembly.addBlock(-3)
    assembly.addLine(['.section .data','','',''])
    assembly.addLine(['integerFormat:\n.ascii "%d\\0"\n',''])
    for variable in allVar:
        assembly.addLine(['LABEL',variable.upper()+':'+'\n'+'.long 0\n'+variable.upper()+'_end',''])
        assembly.addressDescriptor[variable]={'memory':variable.upper(),'register':None}
    assembly.addBlock(-2)
    assembly.addLine(['.section .text',''])
    assembly.addBlock(-1)
    assembly.addLine(['\n','.globl _start',''])
    #printassembly.addressDescriptor
    #printallVar
    for block in sorted(SymbolTable.keys()):
        assembly.addBlock(block)
        #
        # if assembly.labelName != '_start' :
        #     assembly.addLine(['pushl','%ebp',''])
        #     assembly.addLine(['movl','%esp','%ebp',''])

        for programPoint in sorted(SymbolTable[block]):
            #print'\n'
            #printblock, programPoint
            lineCode=assembly.TAC.code[programPoint]
            op=lineCode[1]
            try:
                X=lineCode[2]
            except:
                X=''

            try:
                Y=lineCode[3]
            except:
                Y=''

            try:
                Z=lineCode[4]
            except:
                Z=''
            #printlineCode

            if op == 'ret':
                if assembly.labelName != '_start':
                    assembly.addLine(['movl','%ebp','%esp',''])
                    assembly.addLine(['popl', '%ebp','' ])
                    regR = assembly.getReg(X,programPoint,block)
                    if regR!='%eax':
                        assembly.flushRegister('%eax',block)
                        assembly.addLine(['movl',regR,'%eax',''])
                    assembly.addLine(['ret',''])
            elif op == 'exit':
                assembly.addLine(['call exit',''])
            elif op in ['+','-','*']:
                valY = re.findall(r'\d+',Y)
                if len(valY)!=0 and valY[0] != Y:
                    valY=[]
                # try:
                #     valY = int(Y)
                #     valY = str(valY)
                # except:
                #     valY

                if op=='+':
                    operation='addl'
                elif op=='-':
                    operation='subl'
                else:
                    operation='imull'


                if Y!=[]:
                    if valY!=[]:
                        memY='$'+Y
                    else:
                        memY = assembly.addressDescriptor[Y]['memory']

                if Z!=[]:
                    valZ = re.findall(r'\d+',Z)
                    if len(valZ)!=0 and valZ[0] != Z:
                        valZ=[]
                    if valZ!=[]:
                        memZ='$'+Z
                        if valY!=[]:
                            # this is for X=$2+$3...
                            regX = assembly.getReg(X,programPoint,block)
                            assembly.addLine(['movl',memY,regX,''])
                            assembly.addLine([operation,memZ,regX,''])
                            #No movl is required registerDescriptor and flushRegister takes care of it
                        else:
                            #this is for X=Y+$2
                            regY=assembly.getReg(Y,programPoint,block)
                            assembly.addLine([operation,memZ,regY,''])
                            regX=assembly.addressDescriptor[X]['register']
                            #printregX
                            if regX!=None and regX!=regY:
                                assembly.flushRegister(regX,block)
                            assembly.registerDescriptor[regY]=X
                            assembly.addressDescriptor[Y]['register']=None
                            assembly.addressDescriptor[X]['register']=regY
                            memX=assembly.addressDescriptor[X]['memory']
                            assembly.addLine(['movl',regY,memX,''])
                    else:
                        #this is for X=Y+Z, Y could be a number
                        regZ=assembly.getReg(Z,programPoint,block)
                        assembly.addLine([operation,memY,regZ,''])
                        regX=assembly.addressDescriptor[X]['register']
                        if regX!=None and regX!=regZ:
                            assembly.flushRegister(regX,block)
                        assembly.registerDescriptor[regZ]=X
                        assembly.addressDescriptor[Z]['register']=None
                        assembly.addressDescriptor[X]['register']=regZ
                        memX=assembly.addressDescriptor[X]['memory']
                        assembly.addLine(['movl',regZ,memX,''])

                # if Y!=[]:
                #     if valY!=[]:
                #         regY='$'+Y
                #     else:
                #         regY=assembly.getReg(Y,programPoint,block)
                #
                # if op=='+':
                #     operation='addl'
                # elif op=='-':
                #     operation='subl'
                # else:
                #     operation='imull'
                # assembly.addLine([operation,regY,regZ,''])
                # assembly.addressDescriptor[Z]['register']=None
                # memX = assembly.addressDescriptor[X]['memory']
                # assembly.addLine(['movl',regZ,memX,''])
                # assembly.registerDescriptor[regZ]=X
                # assembly.addressDescriptor[X]['register']=regZ
            elif op == '/':
                assembly.flushAllRegister(block)
                valY = re.findall(r'\d+',Y)
                if len(valY)!=0 and valY[0] != Y:
                    valY=[]

                if Z!=[]:
                    valZ = re.findall(r'\d+',Z)
                    if len(valZ)!=0 and valZ[0] != Z:
                        valZ=[]
                    if valZ!=[]:
                        memZ='$'+Z
                    else:
                        memZ=assembly.addressDescriptor[Z]['memory']

                if Y!=[]:
                    if valY!=[]:
                        memY='$'+Y
                    else:
                        memY=assembly.addressDescriptor[Y]['memory']
                memX=assembly.addressDescriptor[X]['memory']
                assembly.addLine(['movl',memY,'%eax',''])
                assembly.addLine(['movl',memZ,'%ebx',''])
                assembly.addLine(['movl','$0','%edx',''])
                assembly.addLine(['idivl','%ebx',''])
                assembly.addLine(['movl','%eax',memX,''])
            elif op in ['=',':=']:
                valY = re.findall(r'\d+',Y)
                if len(valY)!=0 and valY[0] != Y:
                    valY=[]
                if valY!=[]:
                    regY='$'+Y
                else:
                    regY=assembly.getReg(Y,programPoint,block)
                memX = assembly.addressDescriptor[X]['memory']
                assembly.addLine(['movl',regY,memX,''])
            elif op in ['print']:
                #printX
                assembly.flushAllRegister(block)
                valX = re.findall(r'\d+',X)
                if len(valX)!=0 and valX[0]!=X:
                    valX=[]
                #printvalX
                if valX!=[]:
                    memX='$'+X
                else:
                    #regY=assembly.getReg(Y)
                    memX = assembly.addressDescriptor[X]['memory']
                #printmemX
                # assembly.addLine(['movl','$4','%eax',''])
                # assembly.addLine(['movl','$1','%ebx',''])
                # assembly.addLine(['movl',memX,'%ecx',''])
                # assembly.addLine(['movl','$1','%edx',''])
                # assembly.addLine(['int','$0x80',''])
                assembly.addLine(['pushl',memX,''])
                assembly.addLine(['pushl','$integerFormat',''])
                assembly.addLine(['call','printf',''])
                assembly.addLine(['popl','%eax',''])
                assembly.addLine(['popl','%eax',''])
            elif op in ['read']:
                assembly.flushAllRegister(block)
                X = lineCode[2]
                memX = '$'+assembly.addressDescriptor[X]['memory']
                assembly.addLine(['pushl',memX,''])
                assembly.addLine(['pushl','$integerFormat',''])
                assembly.addLine(['call','scanf',''])
                assembly.addLine(['popl','%eax',''])
                assembly.addLine(['popl','%eax',''])
                # assembly.addLine(['movl','$3','%eax',''])
                # assembly.addLine(['movl','$0','%ebx',''])
                # assembly.addLine(['movl',regY,'%ecx',''])
                # assembly.addLine(['movl','$1','%edx',''])
                # assembly.addLine(['int','$0x80',''])
            elif op in ['if','for']:
                ConditionOp=lineCode[3]
                Y=lineCode[2]
                Z=lineCode[4]
                JumpAdress=lineCode[6]
                #JumpAdress=GetBlockLabel(JumpAdress)  #Kanta we need to write this function
                #leaderJumpAdd = LeaderList[JumpAdress]
                JumpAdress = BasicMapping[int(JumpAdress)]
                valY = re.findall(r'\d+',Y)
                if len(valY)!=0 and valY[0] != Y:
                    valY=[]
                valZ = re.findall(r'\d+',Z)
                if len(valZ)!=0 and valZ[0] != Z:
                    valZ=[]

                if valY!=[]:
                    regY='$'+Y
                else:
                    regY=assembly.getReg(Y,programPoint,block)

                if valZ!=[]:
                    regZ='$'+Z
                else:
                    regZ=assembly.getReg(Z,programPoint,block)
                assembly.addLine(['cmpl',regZ, regY,''])
                if ConditionOp=='=':
                    assembly.addLine(['je',JumpAdress,''])
                if ConditionOp=='<':
                    assembly.addLine(['jl',JumpAdress,''])
                if ConditionOp=='>':
                    assembly.addLine(['jg',JumpAdress,''])
                if ConditionOp=='<=':
                    assembly.addLine(['jle',JumpAdress,''])
                if ConditionOp=='>=':
                    assembly.addLine(['jge',JumpAdress,''])

            elif op in ['call']:
                assembly.addLine(['call',X,''])
                regY = assembly.addressDescriptor[Y]['register']
                if regY==None:
                    regY=assembly.getReg(Y,programPoint,block)
                else:
                    regY=assembly.addressDescriptor[Y]['memory']
                assembly.addLine(['movl','%eax',regY,''])
            elif op=='jmp':
                jmpLine = int(X)
                blockName=BasicMapping[jmpLine]
                assembly.addLine(['jmp',blockName,''])
            #print'\n'
            #printassembly.registerDescriptor
            #printassembly.addressDescriptor

    return assembly
            # for op in ['&']:
            #     addressY = assembly.addressDescriptor[Y]['memory']
            #     regX = assembly.getReg(X,programPoint,block)
            #     assembly.addLine(['movl',addressY,regX,''])
            # for op in ['star']:
            #     regY = assembly.getReg(Y,programPoint,block)
            #     assembly.addLine(['movl',X,regY,''])
