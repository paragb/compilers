	.section .data

	integerFormat:
.ascii "%d\0"


D6:
.long 0
D6_end:

I:
.long 0
I_end:

SUM:
.long 0
SUM_end:

J:
.long 0
J_end:

D2:
.long 0
D2_end:

	.section .text

	
	.globl _start

_start:

	movl	$1,	J

	movl	$1,	SUM

	movl	$0,	I

	movl	$1,	J

	jmp	B5

B0:

	movl	J,	%ecx

	movl	%ecx,	D2

	movl	D2,	%edx

	cmpl	$0,	%edx

	je	B1

	jmp	B3

B1:

	movl	%edx,	D2

	movl	%ecx,	J

	pushl	J

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	jmp	B4

B3:

B3:

	movl	I,	%ecx

	addl	SUM,	%ecx

	movl	%ecx,	SUM

B4:

	movl	I,	%edx

	addl	$1,	%edx

	movl	%edx,	I

	imull	$2,	%edx

	movl	%edx,	J

B5:

	movl	I,	%eax

	cmpl	$99,	%eax

	jle	B0

	movl	%eax,	I

	movl	%edx,	J

	movl	%ecx,	SUM

	pushl	SUM

	pushl	$integerFormat

	call	printf

	popl	%eax

	popl	%eax

	movl	$0,	D6

	call exit

