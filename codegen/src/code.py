import sys
from symtab import *
from generator import *
import re
class ThreeAddressCode:
    def __init__(self):
        self.code = []
        self.readTAC()
    def readTAC(self):
        try:
            file = str(sys.argv[1])
        except:
            file='test/test.3adc'
        raw = open(file,'r').read()
        raw=raw.split('\n')
        cod=[]
        for i in range(len(raw)):
            cod.append(raw[i].split(','))
        self.code = cod

class x86Code:
    def __init__(self, SymbolTable, ThreeAddressCode, LeaderList, BasicMapping, functionList):
        self.code={}
        self.functionList=functionList
        self.ST = SymbolTable
        self.BasicMapping=BasicMapping
        self.LeaderList=LeaderList
        self.TAC = ThreeAddressCode
        self.currentBlock = ""
        self.regCount = 1
        self.NumOfLabels = -1
        self.resetRegisters()
        self.addressDescriptor={}
        self.labelName = ''
    def resetRegisters(self):
        self.registerDescriptor = {
                    '%eax':None,
                    '%ebx':None,
                    '%ecx':None,
                    '%edx':None
                    #TODO: FIll in all registers
                }
        self.freeReg = [ reg for reg in self.registerDescriptor.keys() ]
        self.regInUse = []

    def addLine(self, line):
        self.code[self.currentBlock].append(line)
        #printline

    def addBlock(self,block):
        self.currentBlock=block
        self.code[block]=[]
        if block != -1 and block!=-2 and block!=-3:
            taclineNo = self.LeaderList[int(block)]
            self.labelName = self.BasicMapping[int(taclineNo)]
            if self.labelName == 'main':
                self.labelName='_start'
            self.addLine(['LABEL',self.labelName,':',''])
            if self.BasicMapping[int(taclineNo)] in self.functionList and self.labelName != '_start':
                self.addLine(['pushl','%ebp',''])
                self.addLine(['movl','%esp','%ebp',''])

    def printCode(self,fileName='out'):
        #f = open('output/' + fileName + '.s', 'w')
        for block in sorted(self.code.keys()):
            for i in range(len(self.code[block])):
                assemblyLine = self.code[block][i]
                #printassemblyLine
                if assemblyLine[0] == 'LABEL':
                    print("%s:\n" %assemblyLine[1])
                elif assemblyLine[1]=='':
                    print("\t%s\n" %assemblyLine[0])
                elif assemblyLine[2]=='':
                    print("\t%s\t%s\n" %(assemblyLine[0], assemblyLine[1]))
                elif assemblyLine[3] == '':
                    print("\t%s\t%s,\t%s\n" %(assemblyLine[0], assemblyLine[1], assemblyLine[2]))
                else:
                    print("\t%s\t%s,\t%s,\t%s\n" %(assemblyLine[0], assemblyLine[1], assemblyLine[2], assemblyLine[3]))
        #print("exit:\n\tmovl $1, %eax\n\tmovl $0, %ebx\n\tint $0x80\n")
    def getReg(self, variable,programPoint,block):
        if variable in self.registerDescriptor.values():
            reg = self.addressDescriptor[variable]['register']
            return reg
        else:
            if len(self.freeReg)!=0:
                reg=self.freeReg.pop()
            else:
                maxNextUse=0
                spillVar=''
                spillRegister=''
                for InUseRegister in self.regInUse:
                    InUseRegister_Variable = self.registerDescriptor[InUseRegister]
                    if InUseRegister_Variable != None:
                        InUseRegister_NextUse  = int(self.ST[block][programPoint][InUseRegister_Variable]['NextUse'])
                        maxNextUse = max(maxNextUse,InUseRegister_NextUse)
                        if maxNextUse == InUseRegister_NextUse :
                            spillVar = InUseRegister_Variable
                            spillReg = InUseRegister
                address = self.addressDescriptor[spillVar]['memory']
                self.addLine(['movl',spillReg,address,''])
                self.registerDescriptor[spillReg]=None
                print spillReg
                self.addressDescriptor[spillVar]['register']=None
                self.regInUse.remove(spillReg)
                reg = spillReg
        self.regInUse.append(reg)
        self.registerDescriptor[reg]=variable
        self.addressDescriptor[variable]['register']=reg
        if self.addressDescriptor[variable]['memory'] != None:
            address = self.addressDescriptor[variable]['memory']
            self.addLine(['movl',address,reg,'',''])

        return reg

    def flushRegister(self,reg,block):
        variable = self.registerDescriptor[reg]
        if variable!=None:
            address = self.addressDescriptor[variable]['memory']
            self.addLine(['movl',reg,address,''])
            self.registerDescriptor[reg]=None
            self.addressDescriptor[variable]['register']=None
            self.regInUse.remove(reg)
            self.freeReg.append(reg)

    def flushAllRegister(self,block):
        for reg in self.registerDescriptor.keys():
            variable = self.registerDescriptor[reg]
            if variable!=None:
                address = self.addressDescriptor[variable]['memory']
                self.addLine(['movl',reg,address,''])
                self.registerDescriptor[reg]=None
                self.addressDescriptor[variable]['register']=None
        self.resetRegisters()