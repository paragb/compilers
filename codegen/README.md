GO - Python - X86 Compiler

Following Statements will be in our 3AC output:

1) linenumber, operator, operand, operand, operand (for operator in [-,+,*,/])
	e.g. a=b+c can be written as 10, +, a, b, c

2) Function blocks will start with "func" followed by function name.
	e.g. 10, func, foo

3) Function will end with ret statement.
	e.g 11,ret

4) Atleast one main function will be present and should call exit.
	// main should be called in the beginning.

5) jump to line number statement will be of form:
	12, jmp, linenumber

6) function call will be of form: 13, call, foo, operand where operand is the address in which return value of function should be stored.

7) if statements will be of form : linenumber, if, condition, jmp, linenumber.
	where condition will be of form: a op b (op in [<,>,<=,>=,==])

8) Assignment statement will be of form. linenumber, =, operand,operand.

7) Arrays will be handled in parser.

8) Function call will not take any arguments and return the value in %eax.

9) c functions printf and scanf are imported(library support).

Group Members
=============
 - Akash Waghela (13064)
 - Manikanta Reddy D (13265)
 - Parag Bansal (13464)
 - Shubham Agrawal (13674)
