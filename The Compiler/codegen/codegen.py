import generator
import code
import symtab
TACHere = code.ThreeAddressCode()
symTab = symtab.SymTab(TACHere.code)
symTab.GetBasicBlock()
assemblyCode = generator.generator(symTab,TACHere)
assemblyCode.printCode()
