import code
import re
from symtab import *
def generator(SymTab, ThreeAddressCode):
	SymbolTable=SymTab.SymbolTable
	LeaderList=SymTab.LeaderList
	BasicMapping=SymTab.BasicMapping
	functionList = SymTab.functionList
	assembly=code.x86Code(SymbolTable,ThreeAddressCode,LeaderList,BasicMapping,functionList)
	allVar = [i for i in SymbolTable[0][0].keys()]
	allVar = list(set(allVar))
	# print allVar
	assembly.addBlock(-3)
	assembly.addLine(['\n.section .data','','',''])
	assembly.addLine(['\nintegerFormat:\n.ascii "%d\\n"\n',''])
	for variable in allVar:
		assembly.addLine(['LABEL',variable.upper()+':'+'\n'+'.long 0\n'+variable.upper()+'_end',''])
		assembly.addressDescriptor[variable]={'memory':variable.upper(),'register':None}
	assembly.addBlock(-2)
	assembly.addLine(['\n.section .text',''])
	assembly.addBlock(-1)
	assembly.addLine(['\n','\n.globl _start',''])
	#printassembly.addressDescriptor
	#printallVar

	for block in sorted(SymbolTable.keys()):
		assembly.addBlock(block)		
		#
		# if assembly.labelName != '_start' :
		#	 assembly.addLine(['pushl','%ebp',''])
		#	 assembly.addLine(['movl','%esp','%ebp',''])

		for programPoint in sorted(SymbolTable[block]):
			#print'\n'
			#printblock, programPoint
			lineCode=assembly.TAC.code[programPoint]
			op=lineCode[1]
			try:
				X=lineCode[2]
			except:
				X=''

			try:
				Y=lineCode[3]
			except:
				Y=''

			try:
				Z=lineCode[4]
			except:
				Z=''
			#printlineCode
				
			if op == 'ret':
				if assembly.labelName != '_start':
					assembly.addLine(['movl','%ebp','%esp',''])
					assembly.addLine(['popl', '%ebp','' ])
					regR = assembly.getReg(X,programPoint,block)
					if regR!='%eax':
						assembly.flushRegister('%eax',block)
						assembly.addLine(['movl',regR,'%eax',''])
					assembly.addLine(['ret',''])
			elif op == 'exit':
				assembly.addLine(['call exit',''])

			elif op in ['+','-','*','&&','||','>','<','<=','>=','==','!=']:
				valY = re.findall(r'\d+',Y)
				if len(valY)!=0 and valY[0] != Y:
					valY=[]

				if op=='+':
					operation='addl'
				elif op=='-':
					operation='subl'
				elif op=='*':
					operation='imull'
				elif op=='&&':
					operation='andl'
				elif op=='||':
					operation = 'orl'

				if Y!=[]:
					if valY!=[]:
						memY='$'+Y
						regY='$'+Y
					else:
						memY = assembly.addressDescriptor[Y]['memory']

				if Z!=[]:
					valZ = re.findall(r'\d+',Z)
					if len(valZ)!=0 and valZ[0] != Z:
						valZ=[]
					if valZ!=[]:
						memZ='$'+Z
						if valY!=[]:
							# this is for X=$2+$3...
							if op in ['+','-','*','&&','||']:
								regX = assembly.getReg(X,programPoint,block)
								assembly.addLine(['movl',memY,regX,''])
								assembly.addLine([operation,memZ,regX,''])
								memX=assembly.addressDescriptor[X]['memory']
								assembly.addLine(['movl',regX,memX,''])								

							# May Not Be Valid
							# if op in ['>','<','<=','>=','==','!=']:
							#	 regX=assembly.addressDescriptor[X]['register']
							#	 if regX!=None:
							#		 assembly.flushRegister(regX,block)
							#
							#	 assembly.addLine(['cmpl',memY,memZ,''])
							#	 label = getLabel()
							#
							#	 if op == '>':
							#		 assembly.addLine(['jge',label,''])
							#	 elif op == '<':
							#		 assembly.addLine(['jle',label,''])
							#	 elif op == '>=':
							#		 assembly.addLine(['jg',label,''])
							#	 elif op == '<=':
							#		 assembly.addLine(['jl',label,''])
							#	 elif op == '!=':
							#		 assembly.addLine(['je',label,''])
							#	 elif op == '==':
							#		 assembly.addLine(['jne',label,''])
							#
							#	 memX=assembly.addressDescriptor[X]['memory']
							#	 assembly.addLine(['movl','$1',memX,''])
							#	 assembly.addLine([label+':',''])

							#No movl is required registerDescriptor and flushRegister takes care of it
						else:
							#this is for X=Y+$2
							regY=assembly.getReg(Y,programPoint,block)
							regX=assembly.addressDescriptor[X]['register']
							if regX!=None and regX!=regY:
								assembly.flushRegister(regX,block)
							assembly.registerDescriptor[regY]=X
							assembly.addressDescriptor[Y]['register']=None
							assembly.addressDescriptor[X]['register']=regY
							regX=assembly.addressDescriptor[X]['register']
							memX=assembly.addressDescriptor[X]['memory']

							if op in ['+','-','*','&&','||']:
								assembly.addLine([operation,memZ,regY,''])
								assembly.addLine(['movl',regY,memX,''])
							else:													
								assembly.addLine(['cmpl',memZ,regY,''])
								label = getLabel()
								assembly.addLine(['movl','$0',regX,''])								
								assembly.addLine(['movl',regX,memX,''])
								if op == '>':
									assembly.addLine(['jge',label,''])
								elif op == '<':
									assembly.addLine(['jle',label,''])
								elif op == '>=':
									assembly.addLine(['jl',label,''])
								elif op == '<=':
									assembly.addLine(['jg',label,''])
								elif op == '!=':
									assembly.addLine(['jne',label,''])
								elif op == '==':
									assembly.addLine(['je',label,''])

								assembly.addLine(['movl','$1',regX,''])
								assembly.addLine(['movl',regX,memX,''])
								assembly.addLine([label+':',''])
					else:
						#this is for X=Y+Z, Y could be a number
						regZ=assembly.getReg(Z,programPoint,block)
						regX=assembly.addressDescriptor[X]['register']
						if regX!=None and regX!=regZ:
							assembly.flushRegister(regX,block)
						assembly.registerDescriptor[regZ]=X
						assembly.addressDescriptor[Z]['register']=None
						assembly.addressDescriptor[X]['register']=regZ
						regX=assembly.addressDescriptor[X]['register']
						memX=assembly.addressDescriptor[X]['memory']

						if op in ['+','-','*','&&','||']:
							assembly.addLine([operation,memY,regZ,''])
							assembly.addLine(['movl',regZ,memX,''])

						else:							
							assembly.addLine(['cmpl',regY,regZ,''])							
							label = getLabel()
							assembly.addLine(['movl','$0',regX,''])
							assembly.addLine(['movl',regX,memX,''])
							if op == '>':
								assembly.addLine(['jge',label,''])
							elif op == '<':
								assembly.addLine(['jle',label,''])
							elif op == '>=':
								assembly.addLine(['jl',label,''])
							elif op == '<=':
								assembly.addLine(['jg',label,''])
							elif op == '!=':
								assembly.addLine(['jne',label,''])
							elif op == '==':
								assembly.addLine(['je',label,''])
							assembly.addLine(['movl','$1',regX,''])
							assembly.addLine(['movl',regX,memX,''])
							assembly.addLine([label+':',''])

			elif op in ['/','%']:
				assembly.flushAllRegister(block)
				valY = re.findall(r'\d+',Y)
				if len(valY)!=0 and valY[0] != Y:
					valY=[]

				if Z!=[]:
					valZ = re.findall(r'\d+',Z)
					if len(valZ)!=0 and valZ[0] != Z:
						valZ=[]
					if valZ!=[]:
						memZ='$'+Z
					else:
						memZ=assembly.addressDescriptor[Z]['memory']

				if Y!=[]:
					if valY!=[]:
						memY='$'+Y
					else:
						memY=assembly.addressDescriptor[Y]['memory']
				memX=assembly.addressDescriptor[X]['memory']
				assembly.addLine(['movl',memY,'%eax',''])
				assembly.addLine(['movl',memZ,'%ebx',''])
				assembly.addLine(['movl','$0','%edx',''])
				assembly.addLine(['idivl','%ebx',''])
				if op == '/':
					assembly.addLine(['movl','%eax',memX,''])
				else:
					assembly.addLine(['movl','%edx',memX,''])

			elif op in ['=',':=','=!','=-','=&','=*','*=']:
				valY = re.findall(r'\d+',Y)
				if len(valY)!=0 and valY[0] != Y:
					valY=[]
				if valY!=[]:
					regY='$'+Y
				else:
					regY=assembly.getReg(Y,programPoint,block)
				regX = assembly.getReg(X,programPoint,block)
				memX = assembly.addressDescriptor[X]['memory']
				

				if op == '=':
					assembly.addLine(['movl',regY,regX,''])					
					assembly.addLine(['movl',regX,memX,''])
				elif op == '=!':
					regX = assembly.getReg(X,programPoint,block)
					assembly.addLine(['movl',regY,regX,''])
					assembly.addLine(['notl',regX,''])
					assembly.addLine(['movl',regX,memX,''])
				
				elif op == '=-':
					regX = assembly.getReg(X,programPoint,block)
					assembly.addLine(['movl',regY,regX,''])
					assembly.addLine(['imull',regX,'$-1',''])
					assembly.addLine(['movl',regX,memX,''])
				
				elif op == '=&': #Y is not number
					memY = assembly.addressDescriptor[Y]['memory']					
					assembly.addLine(['movl',regY,memY,'']);					
					assembly.addLine(['movl','$'+str(memY),regX,''])
					memX=assembly.addressDescriptor[X]['memory']
					assembly.addLine(['movl',regX,memX,''])
				
				elif op == '=*': #Y is not number
					regX = assembly.getReg(X,programPoint,block)
					assembly.addLine(['movl','('+str(regY)+')',regX,''])
					assembly.addLine(['movl',regX,memX,''])
				
				elif op == '*=': #Y can be number
					regX = assembly.getReg(X,programPoint,block)
					assembly.addLine(['movl', memX,regX,''])
					assembly.addLine(['movl',regY,'('+regX+')',''])

			elif op in ['print']:
				#printX
				assembly.flushAllRegister(block)
				valX = re.findall(r'\d+',X)
				if len(valX)!=0 and valX[0]!=X:
					valX=[]
				#printvalX
				if valX!=[]:
					memX='$'+X
				else:
					#regY=assembly.getReg(Y)
					memX = assembly.addressDescriptor[X]['memory']

				assembly.addLine(['pushl',memX,''])
				assembly.addLine(['pushl','$integerFormat',''])
				assembly.addLine(['call','printf',''])
				assembly.addLine(['popl','%eax',''])
				assembly.addLine(['popl','%eax',''])
			elif op in ['read']:
				assembly.flushAllRegister(block)
				X = lineCode[2]
				memX = '$'+assembly.addressDescriptor[X]['memory']
				assembly.addLine(['pushl',memX,''])
				assembly.addLine(['pushl','$integerFormat',''])
				assembly.addLine(['call','scanf',''])
				assembly.addLine(['popl','%eax',''])
				assembly.addLine(['popl','%eax',''])

			elif op in ['if','for']:
				Y=lineCode[2]
				JumpAdress=lineCode[4]
				JumpAdress = BasicMapping[int(JumpAdress)]
				valY = re.findall(r'\d+',Y)
				if len(valY)!=0 and valY[0] != Y:
					valY=[]
				if valY!=[]:
					regY='$'+Y
				else:
					memY=assembly.addressDescriptor[Y]['memory']
					regY=assembly.getReg(Y,programPoint,block)
				assembly.addLine(['cmpl', '$1',regY,''])
				assembly.addLine(['jne',JumpAdress,''])


			elif op in ['call']:
				assembly.addLine(['call',X,''])
				regY = assembly.addressDescriptor[Y]['register']
				if regY==None:
					regY=assembly.getReg(Y,programPoint,block)
				else:
					regY=assembly.addressDescriptor[Y]['memory']
				assembly.addLine(['movl','%eax',regY,''])
				assembly.addLine(['movl',regY,memY,''])

			elif op=='jmp':
				assembly.flushAllRegister(block)
				jmpLine = int(X)
				blockName=BasicMapping[jmpLine]
				assembly.addLine(['jmp',blockName,''])

		assembly.flushAllRegister(-1)
			
			#print'\n'
			#printassembly.registerDescriptor
			#printassembly.addressDescriptor

	return assembly
			# for op in ['&']:
			#	 addressY = assembly.addressDescriptor[Y]['memory']
			#	 regX = assembly.getReg(X,programPoint,block)
			#	 assembly.addLine(['movl',addressY,regX,''])
			# for op in ['star']:
			#	 regY = assembly.getReg(Y,programPoint,block)
			#	 assembly.addLine(['movl',X,regY,''])

def retVal(Y):
	valY = re.findall(r'\d+',Y)
	if len(valY)!=0 and valY[0] != Y:
		valY=[]
	if valY!=[]:
		regY='$'+Y
	else:
		regY=assembly.getReg(Y,programPoint,block)
	return regY
labelCounter = 0
def getLabel():
	global labelCounter
	labelCounter+=1
	return 'L'+str(labelCounter)
