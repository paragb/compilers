	
.section .data
	
integerFormat:
.ascii "%d\n"

F:
.long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
F_end:
I:
.long 0
I_end:
T4:
.long 0
T4_end:
T2:
.long 0
T2_end:
T3:
.long 0
T3_end:
T1:
.long 0
T1_end:
T:
.long 0
T_end:
	
.section .text
	
	
.globl _start
_start:
	movl	I,	%ecx
	movl	$0,	%ecx
	movl	%ecx,	I
	movl	%ecx,	I
B0:
	movl	I,	%ecx
	addl	$1,	%ecx
	movl	%ecx,	I
	cmpl	$10,	%ecx
	movl	$0,	%ecx
	movl	%ecx,	T
	jge	L1
	movl	$1,	%ecx
	movl	%ecx,	T
	L1:
	cmpl	$1,	%ecx
	jne	B1
	movl	F,	%edx
	movl	T1,	%eax
	movl	%edx,	F
	movl	$F,	%eax
	movl	%eax,	T1
	movl	I,	%ebx
	imull	$4,	%ebx
	movl	%ebx,	T2
	movl	%eax,	T1
	addl	T1,	%ebx
	movl	%ebx,	T1
	movl	I,	%eax
	movl	T1,	%ebx
	movl	%eax,	(%ebx)
	movl	%ebx,	T1
	movl	%eax,	I
	movl	%edx,	F
	movl	%ecx,	T
	jmp	B0
B1:
	movl	F,	%ecx
	movl	T3,	%edx
	movl	%ecx,	F
	movl	$F,	%edx
	movl	%edx,	T3
	movl	T2,	%eax
	movl	$8,	%eax
	imull	$4,	%eax
	movl	%eax,	T2
	movl	%edx,	T3
	addl	T3,	%eax
	movl	%eax,	T3
	movl	T4,	%edx
	movl	(%eax),	%edx
	movl	%edx,	T4
	movl	%eax,	T3
	movl	%edx,	T4
	movl	%ecx,	F
	pushl	T4
	pushl	$integerFormat
	call	printf
	popl	%eax
	popl	%eax
	call exit
exit:
	movl $1, %eax
	movl $0, %ebx
	int $0x80
