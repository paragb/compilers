#!/usr/bin/env python

#Symbol Table has been designed for data-types, Identifiers and procedures

#Class Table is a standard symbol table
# It is a dictionary of dictionaries..
#

#Width for standard data types
width = {'INT':4, 'FLOAT':8, 'CHAR':1, 'BOOL':4} #8- bit characters

def getWidthOfType(name):
	global width
	if (isinstance(name , str)):
		return width[name.upper()]
	else:
		if(name["type"] in width):
			return width[name["type"]]
		else:
			if "width" in name:
				return name["width"]
			else:
				print name
				print "[Data Type] Error : What Type?"
				return 0


def getProcedureName(name , varList = None):
	str = ""
	str = str + name.lower()
	#Need to Add Overloading features here
	return str

class Table:
	def __init__(self , prev = None):
		self.Entries = {} # Empty Dictionary
		self.width =  0
		self.parameterWidth = 0
		self.previousTable = prev #A Table Block

	def createSymbol(self, name , attributeList):
		if name in self.Entries:
			print 'Error : Symbol -' + name + '- Already present in the symbol table'
		else :
			self.Entries[name] = {}
			for item in attributeList :
				self.Entries[name][item] = attributeList[item]

	def updateSymbol(self, name , attributeName , Value):
		currentTable = self
		while currentTable != None:
			if name in currentTable.Entries:
				(currentTable.Entries[name])[attributeName] = Value # Adding Correcponding value
				return True
			currentTable = currentTable.previousTable

		return False

	def locateSymbolHere(self , name):
		if name in self.Entries:
			return True
		else:
			return False

	def locateSymbol(self , name) :
		currentTable = self
		while currentTable != None :
			if currentTable.locateSymbolHere(name):
				return True
			else:
				currentTable = currentTable.previousTable
		return False

	def getEntriesTable(self):
		return self.Entries

	def getRow(self , name) :
		currentTable = self

		while(currentTable != None):
			if(currentTable.locateSymbolHere(name) == True):
				return currentTable.Entries[name]

			currentTable = currentTable.previousTable

		return None


	def getAttributeValInBlock(self, name , attributeName):
		if name in self.Entries:
			if attributeName in self.Entries[name]:
				   return self.Entries[name][attributeName]
			else:
				return None
		else:
			return None

	def getAttributeVal(self , name , attributeName):
		currentTable = self
		while currentTable != None :
			value = currentTable.getAttributeValInBlock(name , attributeName)
			if (value != None) :
				return value
			else:
				currentTable = currentTable.previousTable

		return None


	def printSymbolTable(self):
		print "********* Printing the Symbol Table " + str(self) + "*********"
		for item in self.Entries:
			if "SymbolTable" in self.Entries[item]:
				print "+++++++++++++++++++++++++++++++++++++++"
				print "printing the sub symbol table for " +  item
				print "+++++++++++++++++++++++++++++++++++++++"
				self.Entries[item]["SymbolTable"].printSymbolTable()
				print "---------------------------------------"
			print item + " ==> " + str(self.Entries[item])

	def getWidth(self):
		# '''
		# Helper function. Return width
		# '''
		return self.width

	def changeWidth(self, change) :
		self.width = self.width + change

	def getParameterWidth(self):
		return self.parameterWidth

	def changeParameterWidth(self , change):
		self.parameterWidth = self.parameterWidth + change

class SymbolTable:
	def __init__(self):
		self.symbolTable = Table()

	def createSymbol(self, name , attributeList):
		self.symbolTable.createSymbol(name , attributeList)

	def updateSymbol(self, name , attributeName , Value):
		return self.symbolTable.updateSymbol(name , attributeName , Value)

	def getAttributeVal(self , name , attributeName):
		return self.symbolTable.getAttributeVal(name , attributeName)

	def locateSymbolHere(self , name):
		return self.symbolTable.locateSymbolHere(name)

	def locateSymbol(self , name) :
		return self.symbolTable.locateSymbol(name)

	def getEntriesTable(self):
		return self.symbolTable.getEntriesTable()

	def getRow(self , name) :
		return self.symbolTable.getRow(name)

	def printSymbolTable(self):
		self.symbolTable.printSymbolTable()

	def getWidth(self):
		return self.symbolTable.getWidth()

	def changeWidth(self, change) :
		self.symbolTable.changeWidth(change)

	def getParameterWidth(self):
		return self.symbolTable.getParameterWidth()

	def changeParameterWidth(self , change):
		self.symbolTable.changeParameterWidth(change)

	def getCurrentTable(self):
		return self.symbolTable

	def setCurrentTable(self , tableObject):
		self.symbolTable = tableObject

	def startScope(self):
		newTable = Table(self.symbolTable)
		self.symbolTable = newTable
		return self.symbolTable


	def finishScope(self):
		self.symbolTable = self.symbolTable.previousTable


	def makeProcedure(self , name , varList):
		old_name = name
		name = getProcedureName(name , varList)

		if self.symbolTable.previousTable == None :
			print "Function not Defined within a global scope - Some Problem with the declaration of " + "old_name"
		else :
			self.symbolTable.previousTable.createSymbol(name , {"isprocedure" : True , "SymbolTable" : self.symbolTable,  "varList" : varList , "value" : old_name , "offset" : self.symbolTable.previousTable.getWidth() , 'isbase':False}) # in_List and out_List are not required as such

			for item in varList :
				self.symbolTable.createSymbol(item , varList[item])
				self.symbolTable.updateSymbol(item , "offset" , self.symbolTable.getWidth())

