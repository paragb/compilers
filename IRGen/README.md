 # IR GEN

 ## Attribute rules

### identifier_list
>  `identifier_list : IDENTIFIER
                      | IDENTIFIER COMMA identifier_list`


### operand
> `operand -> literal | operand_name | ( expression )`
 * value (1 , a)
 * type  ( IDENTIFER or number)
 * isbase


### operand_name
> `operand_name -> IDENTIFER | qualified_ident*`
 + value (a)
 + type  (IDENTIFER)
 + isbase

### qualified_ident
> `qualified_ident : IDENTIFIER DOT IDENTIFIER`




### expression
> `expression : unary_expr | expression binary_op expression`
 + value (a or 1 or a+b)
 + type
 + isbase


### unary_expr
> `unary_expr  : primary_expr | LPAREN unary_op unary_expr RPAREN`
 + value  
 + type
 + Note to Self: unary_op gives a token and implemented!
 + caret,

### expression_list
> `expression_list : expression comma_expression`
 + e1, e2, e3 ...
   + e1 (essential expression)
    + value (a or 1 or a+b)
    + type
    + isbase



### primary_expr
> `primary_expr : Operand
                  | primary_expr index*
                  | primary_expr arguments*
                  | primary_expr selector*`
 + value
 + type
 + isbase
 + Note to Self: index, arguments, selector are ensured to be vti's



### literal
> ` literal -> basic_lit | function_lit* `
 + value (1)
 + type (FLOAT | INT ~ number)
 + isbase
