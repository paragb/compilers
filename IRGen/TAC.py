
from mySymTab import *
Debug1 = False
Debug3 = False


symbol_table = SymbolTable()



# symbol_table.createSymbol('integer',{'type': 'INT', 'lexeme':'integer', 'istype':True , 'width' : 4})
# symbol_table.createSymbol('float',{'type': 'FLOAT', 'lexeme':'float', 'istype':True , 'width' : 8})
# symbol_table.createSymbol('character',{'type': 'CHAR', 'lexeme':'character', 'istype':True , 'width' : 1})
# #symbol_table.createSymbol('string',{'type': 'STRING', 'lexeme':'string', 'istype':True , 'width' : None})
# symbol_table.createSymbol('boolean',{'type': 'BOOL', 'lexeme':'boolean', 'istype':True , 'width' : 4})

tmpCounter = 0



def getTempNum():
	global tmpCounter
	tmpCounter += 1
	return tmpCounter


def getTemp(type): #Makes a new temperory variable and then returns its name
	name = '_t' + str(getTempNum())
	symbol_table.createSymbol(name , {"type" : type , "width" : getWidthOfType(type) , "value" : name , "isvar" : True, "isbase":True})
	symbol_table.changeWidth(getWidthOfType(type))
	return name

# 3 Address code list TAC
class TAC:
    def __init__(self):
        self.nextInstNum = 0
        self.TAcode = []

    def emitCode(self, result , operand1 , operator , operand2):
    	self.TAcode.append([str(self.nextInstNum)  , str(operator) , str(result) , str(operand1), str(operand2)])
    	self.nextInstNum += 1

    def getNextInstNum(self):
    	return  self.nextInstNum

    def printTACs(self):
    	print "========== Three Address Code ==============="
    	for i in self.TAcode:
			code = ''
			# print i
			if i[2]=='jmp':
				if i[3] == 'None': code = i[0]+','+i[2]+','+i[1]+','
				else: code = i[0]+','+i[2]+','+i[3]+','
			else:
				for j in i:
					if j != 'None':	code+=str(j)+','
			print code[:-1]

threeAddrCode = TAC()

def makeList(i):
	return [i]

def merge(L1 , L2):
	return L1 + L2

def backpatch(L1 , instNum):
    for item in L1:
        mylist = threeAddrCode.TAcode[item]
        for i in range(0,5):
            if(mylist[i] == 'None'):
                mylist[i] = str(instNum)
                break
