#!/usr/bin/python
import sys
sys.path.insert(0, '../lexical_analyzer/src')
sys.path.insert(0, '../')
from ply import yacc
from ply import lex
from lexer import *
########################################
############# STATEMENTS ###############
########################################
def p_start(p):
    '''start : pkg_definition program
    '''
def p_program(p):
    '''program : imports program
                | top_level_decl program
                | empty
    '''
def p_empty(p):
    '''empty :'''
    pass
def p_pkg_definition(p):
    '''pkg_definition : PACKAGE IDENTIFIER
    '''
def p_imports(p):
    '''imports : IMPORT STRING
    '''
def p_top_level_decl(p):
    '''top_level_decl : declaration
                        | function_decl
    '''
def p_declaration(p):
    '''declaration : const_decl
                    | type_decl
                    | var_decl
    '''
def p_var_decl(p):
    '''var_decl  : VAR var_spec
    '''
def p_var_spec(p):
    '''var_spec  : identifier_list var_spec_part'''
def p_var_spec_part(p):
    '''var_spec_part : type 
                    |  type EQUAL expression_list
                    |  EQUAL expression_list
    '''
def p_const_decl(p):
    '''const_decl : CONSTANT const_spec
    '''
def p_const_spec(p):
    '''const_spec : identifier_list const_spec_part
    '''
def p_const_spec_part(p):
    '''const_spec_part : EQUAL expression_list
                        | type EQUAL expression_list
    '''
def p_type_decl(p):
    '''type_decl  :  TYPE type_spec
    '''
def p_type_spec(p):
    '''type_spec  : IDENTIFIER type
    '''
def p_identifier_list(p):
    '''identifier_list : IDENTIFIER 
                        | IDENTIFIER COMMA identifier_list
    '''
# def p_comma_identifier(p):
#     '''comma_identifier : COMMA IDENTIFIER comma_identifier
#                         | empty
#     '''
def p_expression_list(p):
    '''expression_list : expression comma_expression
    '''
def p_comma_expression(p):
    '''comma_expression : COMMA expression comma_expression
                        | empty
    '''
def p_type(p):
    ''' type    : array_type
                | struct_type
                | pointer_type
                | BASETYPE
    '''
def p_array_type(p):
    '''array_type   : LBRACK array_length RBRACK element_type
    '''
def p_array_length(p):
    '''array_length : expression
                    | empty
    '''
def p_element_type(p):
    '''element_type : type
    '''
def p_struct_type(p):
    '''struct_type : STRUCT LBRACE struct_type_part RBRACE
    '''
def p_struct_type_part(p):
    '''struct_type_part : field_decl SEMICOL struct_type_part
                        | field_decl SEMICOL
    '''
def p_pointer_type(p):
    ''' pointer_type : TIMES type
    '''
def p_field_decl(p):
    '''field_decl : identifier_list type
    '''
def p_expression(p):
    '''expression : unary_expr
                    | expression binary_op expression'''
def p_unary_expr(p):
    '''unary_expr  : primary_expr
                    | unary_op unary_expr'''
def p_primary_expr(p):
    '''primary_expr : Operand
                    | conversion
                    | primary_expr selector
                    | primary_expr index
                    | primary_expr type_assertion
                    | primary_expr arguments
    '''
def p_selector(p):
    '''selector      : DOT IDENTIFIER
    '''
def p_index(p):
    '''index         : LBRACK expression RBRACK
    '''
#Can be skiped
def p_type_assertion(p):
    '''type_assertion : DOT LPAREN type RPAREN'''
def p_arguments(p):
    '''arguments     : LPAREN arguments_part RPAREN
    '''
def p_arguments_part(p):
    '''arguments_part : expression_list
                        | type arguments_part_two
                        | empty
    '''
def p_arguments_part_two(p):
    ''' arguments_part_two : COMMA expression_list
                            | empty
    '''
def p_conversion(p):
    '''conversion : type LPAREN expression RPAREN
    '''
def p_operand(p):
        '''Operand  : literal
                        | operand_name
                        | LPAREN expression RPAREN
        '''
def p_literal(p):
    '''literal     : basic_lit
                    | function_lit
    '''
def p_basic_lit(p):
    '''basic_lit    : INTEGER
                    | FLOAT
                    | STRING '''
def p_func_lit(p):
    '''function_lit : FUNCTION function
    '''
def p_operand_name(p):
    '''operand_name : IDENTIFIER
                    | qualified_ident'''
def p_qualified_ident(p):
    '''qualified_ident : IDENTIFIER DOT IDENTIFIER '''
def p_unary_op(p):
    ''' unary_op : PLUS
                    | MINUS
                    | CARET
                    | NOT
                    | TIMES
                    | AMPERS
                    | LMINUS
    '''
def p_binary_op(p):
    '''binary_op :  AMPAMP
                    | OROR
                    | rel_op
                    | add_op
                    | mul_op
    '''
def p_rel_op(p):
    '''rel_op     : EQEQ
                    | NOTEQ
                    | LESS
                    | LEQ
                    | GREAT
                    | GEQ
    '''
def p_add_op(p):
    '''add_op   : PLUS
                    | MINUS
                    | OR
                    | CARET
    '''
def p_mul_op(p):
    '''mul_op   : TIMES
                    | DIVIDE
                    | MOD
                    | LL
                    | GG
                    | AMPERS
                    | AMPCAR
    '''
def p_signature(p):
    '''signature : parameters result '''
def p_result(p):
    '''result  : parameters
                | type
                | empty
    '''
def p_parameter(p):
    '''parameters : LPAREN parameter_part RPAREN
    '''
def p_parameter_part(p):
    ''' parameter_part : parameter_list
                        | empty
    '''
def p_parameter_list(p):
    '''parameter_list  : parameter_decl parameter_list_part
    '''
def p_parameter_list_part(p):
    ''' parameter_list_part : COMMA parameter_decl parameter_list_part
                            | empty
    '''
def p_parameter_decl(p):
    '''parameter_decl  : parameter_decl_part type
    '''
def p_parameter_decl_part(p):
    ''' parameter_decl_part : identifier_list
                            | empty
    '''
def p_func_decl(p):
    '''function_decl : FUNCTION function_name func_decl_part
    '''
def p_func_decl_part(p):
    ''' func_decl_part : function
                        | signature
    '''
def p_func_name(p):
    '''function_name : IDENTIFIER
    '''
def p_function(p):
    '''function  : signature function_body
    '''
def p_function_body(p):
    '''function_body : block
    '''
def p_block(p):
    '''block : LBRACE statement_list RBRACE
    '''
def p_statement_list(p):
    '''statement_list : statement_list_part
    '''
def p_statement_list_part(p):
    '''statement_list_part : statement statement_list_part
                            | statement 
    '''
def p_statement(p):
    '''statement : declaration
                | labeled_stmt
                | simple_stmt
                | go_stmt
                | return_stmt
                | break_stmt
                | continue_stmt
                | go_to_stmt
                | block
                | if_stmt
                | switch_stmt
                | for_stmt
    '''
def p_simple_stmt(p):
    '''simple_stmt : expression_stmt
                    | inc_dec_stmt
                    | assignment
    '''
def p_label_stmt(p):
    '''labeled_stmt : label COLON statement
    '''
def p_label(p):
    '''label       : IDENTIFIER
    '''
def p_expression_stmt(p):
    '''expression_stmt : expression
    '''
def p_inc_dec_stmt(p):
    '''inc_dec_stmt : expression inc_dec_stmt_part '''
def p_inc_dec_stmt_part(p):
    '''inc_dec_stmt_part : PLUSPLUS
                        | MINUSMIN
    '''
def p_assignment(p):
    '''assignment : expression_list assign_op expression_list
    '''
def p_assign_op(p):
    '''assign_op : assign_op_part EQUAL
    '''
def p_assign_op_part(p):
    ''' assign_op_part : add_op
                        | mul_op
                        | empty
    '''
def p_if_stmt(p):
    '''if_stmt : IF expression block if_stmt_part2
    '''
# def p_if_stmt_part1(p):
#     ''' if_stmt_part1 : simple_stmt SEMICOL
#                         | empty
#     '''
def p_if_stmt_part2(p):
    ''' if_stmt_part2 : ELSE if_stmt_part3
                        | empty
    '''
def p_if_stmt_part3(p):
    ''' if_stmt_part3 : if_stmt
                        | block
    '''
def p_switch_stmt(p):
    ''' switch_stmt : expr_switch_stmt
    '''
def p_expr_switch_stmt(p):
    '''expr_switch_stmt : SWITCH expr_switch_stmt_part1 LBRACE expr_switch_stmt_part2 RBRACE
    '''
    # '''expr_switch_stmt : SWITCH if_stmt_part1 expr_switch_stmt_part1 LBRACE expr_switch_stmt_part2 RBRACE
    # '''
def p_expr_switch_stmt_part1(p):
    '''expr_switch_stmt_part1 : expression
                            | empty
    '''
def p_expr_switch_stmt_part2(p):
    '''expr_switch_stmt_part2 : expr_case_clause expr_switch_stmt_part2
                                | empty
    '''
def p_expr_case_clause(p):
    '''expr_case_clause : expr_switch_case COLON statement_list
    '''
def p_expr_switch_case(p):
    '''expr_switch_case : CASE expression_list
                            | DEFAULT
    '''
def p_for_stmt(p):
    '''for_stmt : FOR for_stmt_part block
    '''
def p_for_stmt_part(p):
    ''' for_stmt_part : condition
                        | for_clause
    '''
def p_for_clause(p):
    '''for_clause :  init_stmt  SEMICOL  condition  SEMICOL  post_stmt
    '''
def p_init_stmt(p):
    '''init_stmt : simple_stmt
    '''
def p_post_stmt(p):
    '''post_stmt : simple_stmt
    '''
def p_condition(p):
    '''condition : expression
                | empty
    '''
# def p_range_clause(p):
#     '''range_clause : range_clause_part RANGE expression
#     '''
#
# def p_range_clause_part(p):
#     ''' range_clause_part : expression_list EQUAL
#                             | identifier_list COLONEQ
#                             | empty
#     '''
def p_go_stmt(p):
    '''go_stmt : GO expression
    '''
def p_go_to_stmt(p):
    '''go_to_stmt : GOTO IDENTIFIER
    '''
def p_return_stmt(p):
    '''return_stmt : RETURN expr_switch_stmt_part1'''
def p_break_stmt(p):
    '''break_stmt : BREAK break_stmt_part1
    '''
def p_break_stmt_part1(p):
    ''' break_stmt_part1 : label
                        | empty
    '''
def p_continue_stmt(p):
    '''continue_stmt : CONTINUE break_stmt_part1
    '''
########################################
############# ERROR ####################
########################################
def p_error(p):
    print "line :",p.lineno,"-Parsing Error Found at Token:",p.type
    parser.errok()
old_stderr = sys.stderr
parser = yacc.yacc(start = 'start', debug = True)
##############################################################################
import sys,re
def make_ParseTree(file_name):
    f = open("intermediate_dot","r")
    f = f.read()
    f = re.findall(r'Reduce rule \[(.*?)\] with',f)
    f.reverse()
    productions = [f[i].split(' -> ') for i in range(len(f))]
    derv = productions[0][0]
    file_name = file_name.split(".")[0]+".html"
    htm = open(file_name,"w")
    for i in range(len(productions)):
        # print derv
        htm.write(derv+"<br><br>")
        derv=derv.replace("<empty>","")
        derv=derv.replace("<b>","")
        derv=derv.replace("</b>","")
        revdev = derv[::-1]
        #print revdev
        toberep = productions[i][0][::-1]
        #print toberep
        replacem = "<b>"+productions[i][1]+"</b>"
        replacem = replacem[::-1]
        #print replacem
        revdev = revdev.replace(toberep,replacem,1)
        #print revdev
        derv = revdev[::-1]
        # htm.write("Using Prooduction:        "+f[i]+"<br>")
        # print derv
    # print derv
    htm.write(derv+"<br>")
    print "Writing html"
    htm.close()
#Scanning the file name
if (len(sys.argv) == 1):
    file_name =raw_input( "Give an Ada file to parse: ")
else:
    file_name = sys.argv[1]
try:
    lexer = lex.lex()
    with open(file_name) as fp:#opening file
        data = fp.read()
        parser = yacc.yacc(start = 'start', debug = True)
        lexer.input(data)
        sys.stderr = open("intermediate_dot" , 'w')
        result = parser.parse(data , debug = 1)
        sys.stderr = old_stderr
        make_ParseTree(file_name)
except IOError as e:
    print "I/O error({0}): "+ "We are not able to open " + file_name + " . Does it Exists? Check permissions!"