// `for` is Go's only looping construct. Here are
// three basic types of `for` loops.

package main

import "fmt"

func main() {
    var i int ;
    // The most basic type, with a single condition.
    i = 1 ;
    for i = 3; i<=4; i++ {
        fmt.Println(i) ;
        i = i + 1 ;
    } ;
    var j int ;
    // A classic initial/condition/after `for` loop.
    for j = 7; j <= 9; j++ {
        fmt.Println(j) ;
    } ;

    // `for` without a condition will loop repeatedly
    // until you `break` out of the loop or `return` from
    // the enclosing function.
    for j = 7; j <= 9; j++ {
        fmt.Println("loop") ;
        break ;
    } ;
}
