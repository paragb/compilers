// Go's _structs_ are typed collections of fields.
// They're useful for grouping data together to form
// records.

package main

import "fmt"

// This `person` struct type has `name` and `age` fields.
type person struct {
    name string ;
    age  int ;
}

func main() {
    fmt.Println() ;
    a=b ;
    //fmt.Println(person{name: "Alice", age: 30})
    fmt.Println(person->name, person->age) ;
    //fmt.Println(&person{name: "Ann", age: 40})
    //s = person{name: "Sean", age: 50}
    fmt.Println(s->name) ;
    fmt.Println(sp->age) ;
    sp = (&s) ;
    sp->age = 51 ;
    fmt.Println(sp->age) ;
}
