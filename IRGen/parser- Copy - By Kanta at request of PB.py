#!/usr/bin/python
import sys
sys.path.insert(0, '../lexical_analyzer/src')
sys.path.insert(0, '../')
from ply import yacc
from ply import lex
from lexer import *
from copy import deepcopy
from TAC import *
########################################
############# STATEMENTS ###############
########################################
debug= True
precedence = (
    ('left','OROR'),
    ('left','AMPAMP'),
    ('left','EQEQ', 'NOTEQ', 'LESS','LEQ','GREAT','GEQ'),
    ('left', 'PLUS', 'MINUS','OR','CARET'),
    ('left', 'TIMES', 'DIVIDE', 'MOD', 'LL', 'GG', 'AMPERS', 'AMPCAR' ),
)

def p_start(p):
    '''start : pkg_definition program
    '''
    print "DONE"

def p_program(p):
    '''program : imports program
                | top_level_decl program
                | empty
    '''

def p_empty(p):
    '''empty :'''
    p[0]=None

def p_pkg_definition(p):
    '''pkg_definition : PACKAGE IDENTIFIER
    '''

def p_imports(p):
    '''imports : IMPORT STRING
    '''

def p_top_level_decl(p):
    '''top_level_decl : declaration
                        | function_decl
    '''

def p_declaration(p):
    '''declaration : const_decl
                    | type_decl
                    | var_decl
    '''

def p_var_decl(p):
    '''var_decl  : VAR var_spec
    '''

def p_var_spec(p):
    '''var_spec  : identifier_list type'''
    for i in p[1]:
        symbol_table.createSymbol(i,{"value": i, "type": p[2], "isbase": True, "isvar":True, "Width": getWidthOfType(p[2])})
        if (debug):
            print "Declared: "+ i+" Of type: "+p[2]

# def p_var_spec_part(p):
#     '''var_spec_part : type
#                     |  type EQUAL expression_list
#                     |  EQUAL expression_list
#     '''

def p_const_decl(p):
    '''const_decl : CONSTANT const_spec
    '''

def p_const_spec(p):
    '''const_spec : identifier_list const_spec_part
    '''


def p_const_spec_part(p):
    '''const_spec_part : EQUAL expression_list
                        | type EQUAL expression_list
    '''

def p_type_decl(p):
    '''type_decl  :  TYPE type_spec
    '''

def p_type_spec(p):
    '''type_spec  : IDENTIFIER type
    '''

def p_identifier_list(p):
    '''identifier_list : IDENTIFIER
                        | IDENTIFIER COMMA identifier_list
    '''
    p[0]=[]
    if len(p)==2:
        p[0].append(p[1])
    else:
        p[0]=deepcopy(p[3])
        p[0].append(p[1])

# def p_comma_identifier(p):
#     '''comma_identifier : COMMA IDENTIFIER comma_identifier
#                         | empty
#     '''

def p_expression_list(p):
    '''expression_list : expression comma_expression
    '''
    # print p[2], p[1]
    temp=len(p[2])
    string='e_'+str(temp+1)
    p[0]=deepcopy(p[2])
    p[0][string]=p[1]

def p_comma_expression(p):
    '''comma_expression : COMMA expression comma_expression
                        | empty
    '''
    if(len(p)==2):
        p[0]={}
    else:
        temp=len(p[3])
        string='e_'+str(temp+1)
        p[0]=deepcopy(p[3])
        p[0][string]=p[2]

def p_type(p):
    ''' type    : array_type
                | struct_type
                | pointer_type
                | BASETYPE
    '''
    p[0]=p[1]

def p_array_type(p):
    '''array_type   : LBRACK array_length RBRACK element_type
    '''

def p_array_length(p):
    '''array_length : expression
                    | empty
    '''

def p_element_type(p):
    '''element_type : type
    '''

def p_struct_type(p):
    '''struct_type : STRUCT LBRACE struct_type_part RBRACE
    '''

def p_struct_type_part(p):
    '''struct_type_part : field_decl SEMICOL struct_type_part
                        | field_decl SEMICOL
    '''

def p_pointer_type(p):
    ''' pointer_type : TIMES type
    '''

def p_field_decl(p):
    '''field_decl : identifier_list type
    '''

def p_expression(p):
    '''expression : unary_expr
                    | expression binary_op expression
    '''
    p[0]={}
    if (len(p)==2):
        p[0]=p[1]
    else:
        if(p[2] in ['==', '!=', '<', '<=' , '>' , '>=']):
            thistype='BOOL'
            # if((p[1]['type']=='INT' and p[3]['type']!='INT') or (p[1]['type']!='INT' and p[3]['type']=='INT') ):
            #     p_error(p)
            # elif((p[1]['type']=='FLOAT' and p[3]['type']!='FLOAT') or (p[1]['type']!='FLOAT' and p[3]['type']=='FLOAT')):
            #     p_error(p)
            # elif((p[1]['type']=='FLOAT') and )
            if p[1]['type'] in ['INT','FLOAT','IDENTIFIER'] and p[3]['type'] in ['INT','FLOAT','IDENTIFIER']:
                pass
            else:
                p_error(p)
            p[0]['value']=getTemp(thistype)
            p[0]['type']='BOOL'
            p[0]['isvar']='True'
            p[0]['isbase']='True'
            p[0]['Width']= getWidthOfType(thistype)
            threeAddrCode.emitCode(p[0]['value'],p[1]['value'],p[2],p[3]['value'])
            if (debug):
                print (p[0]['value'],p[1]['value'],p[2],p[3]['value'])
        elif (p[2] in ['+','*','-']):
            if (p[1]['type'] not in ['INT','FLOAT']) or (p[3]['type'] not in ['INT','FLOAT']):
                p_error(p)
            else:
                if p[1]['type'] == 'FLOAT' or p[3]['type'] == 'FLOAT':
                    thistype='FLOAT'
                else:
                    thistype='INT'
            p[0]['value']=getTemp(thistype) #check if correct.
            p[0]['type']=thistype
            p[0]['isvar']='True'
            p[0]['isbase']='True'
            p[0]['Width']= getWidthOfType(thistype)
            threeAddrCode.emitCode(p[0]['value'],p[1]['value'],p[2],p[3]['value'])
            if (debug):
                print (p[0]['type'],p[0]['value'],p[1]['value'],p[2],p[3]['value'])

# def p_expression(p):
#     '''expression : unary_expr
#                     | expression binary_op expression
#     '''
#     if (len(p)==2):
#         p[0]=p[1]
#     else:
#         if(p[2] in ['==', '!=', '<', '<=' , '>' , '>=']):
#             type='BOOL'
#             if(p[1]['type']!='BOOL' or p[3]['type']!='BOOL'):
#                 p_error(p)
#         if(p[2] in [PLUS, ])
#         p[0]['value']=getTemp(type)
#         threeAddrCode.emitCode(p[0]['value'],p[1]['value'],p[2],p[3]['value'])
#         if (debug):
#             print (p[0]['value'],p[1]['value'],p[2],p[3]['value'])


def p_unary_expr(p):
    '''unary_expr  : primary_expr
                    | LPAREN unary_op unary_expr RPAREN
    '''
    if (len(p)==2):
        p[0]=p[1]
    else :
        p[0]=depcopy(p[3])
        p[0]['value']=getTemp()
        threeAddrCode.emitCode(p[0]['value'],p[3]['value'], p[2])
        if(debug):
            print (p[0]['value'],p[3]['value'], p[2])

def p_primary_expr(p):
    '''primary_expr : Operand
                    | primary_expr index
                    | primary_expr arguments
                    | primary_expr selector
    '''
    if (len(p)==2):
        p[0]=p[1]
    else :
        #array ka code generate
        #Struct Ka code
        #p[0]={"value":}
        pass

def p_selector(p):
    '''selector      : MING IDENTIFIER
    '''
    p[0]=p[2]
def p_index(p):
    '''index         : LBRACK expression RBRACK
    '''
    p[0]=p[2]

#Can be skiped
def p_type_assertion(p):
    '''type_assertion : DOT LPAREN type RPAREN'''

def p_arguments(p):
    '''arguments     : LPAREN arguments_part RPAREN
    '''
def p_arguments_part(p):
    '''arguments_part : expression_list
                        | type arguments_part_two
                        | empty
    '''
def p_arguments_part_two(p):
    ''' arguments_part_two : COMMA expression_list
                            | empty
    '''
def p_conversion(p):
    '''conversion : type LPAREN expression RPAREN
    '''

def p_operand(p):
    '''Operand  : literal
                    | operand_name
                    | LPAREN expression RPAREN
    '''
    if (len(p)==2):
        p[0]=p[1]
    else:
        p[0]=p[2]

def p_literal(p):
    '''literal     : basic_lit
                    | function_lit
    '''
    p[0] = p[1]

def p_basic_lit_1(p):
    '''basic_lit    : INTEGER
    '''
    p[0] = {"value": p[1], "type": "INT", "isbase": True, "isvar":False, "Width":4}


def p_basic_lit_2(p):
    '''basic_lit    : FLOAT
    '''
    p[0] = {"value": p[1], "type": "FLOAT", "isbase": True, "isvar":False, "Width":4}


def p_basic_lit_3(p):
    '''basic_lit    : STRING
    '''
    p[0] = {"value": p[1], "type": "STRING", "isbase": True, "isvar":False, "Width":128}


def p_func_lit(p):
    '''function_lit : FUNCTION function
    '''

def p_operand_name(p):
    '''operand_name : IDENTIFIER
                    | qualified_ident
    '''
    p[0]=symbol_table.getRow(p[1])
    # print p[0]

def p_qualified_ident(p):
    '''qualified_ident : IDENTIFIER DOT IDENTIFIER '''

def p_unary_op(p):
    ''' unary_op : PLUS
                    | MINUS
                    | CARET
                    | NOT
                    | TIMES
                    | AMPERS
                    | LMINUS
    '''
    p[0]=p[1]

def p_binary_op(p):
    '''binary_op :  AMPAMP
                    | OROR
                    | rel_op
                    | add_op
                    | mul_op
    '''
    p[0]=p[1]

def p_rel_op(p):
    '''rel_op     : EQEQ
                    | NOTEQ
                    | LESS
                    | LEQ
                    | GREAT
                    | GEQ
    '''
    p[0]=p[1]

def p_add_op(p):
    '''add_op   : PLUS
                    | MINUS
                    | OR
                    | CARET
    '''
    p[0]=p[1]


def p_mul_op(p):
    '''mul_op   : TIMES
                    | DIVIDE
                    | MOD
                    | LL
                    | GG
                    | AMPERS
                    | AMPCAR
    '''
    p[0]=p[1]

def p_signature(p):
    '''signature : parameters result '''

def p_result(p):
    '''result  : parameters
                | type
                | empty
    '''

def p_parameter(p):
    '''parameters : LPAREN parameter_part RPAREN
    '''

def p_parameter_part(p):
    ''' parameter_part : parameter_list
                        | empty
    '''

def p_parameter_list(p):
    '''parameter_list  : parameter_decl parameter_list_part
    '''


def p_parameter_list_part(p):
    ''' parameter_list_part : COMMA parameter_decl parameter_list_part
                            | empty
    '''

def p_parameter_decl(p):
    '''parameter_decl  : parameter_decl_part type
    '''

def p_parameter_decl_part(p):
    ''' parameter_decl_part : identifier_list
                            | empty
    '''

def p_func_decl(p):
    '''function_decl : FUNCTION function_name func_decl_part
    '''

def p_func_decl_part(p):
    ''' func_decl_part : function
                        | signature
    '''

def p_func_name(p):
    '''function_name : IDENTIFIER
    '''

def p_function(p):
    '''function  : signature function_body
    '''

def p_function_body(p):
    '''function_body : block
    '''

def p_block(p):
    '''block : LBRACE statement_list RBRACE
    '''

def p_statement_list(p):
    '''statement_list : statement_list_part
    '''

def p_statement_list_part(p):
    '''statement_list_part : statement SEMICOL statement_list_part
                            | statement SEMICOL
    '''
                # | simple_stmt
                # | go_stmt
                # | return_stmt
                # | break_stmt
                # | continue_stmt
                # | go_to_stmt
                # | block
                # | if_stmt M21
                # | switch_stmt
                # | for_stmt
def p_statement(p):
    '''statement : declaration'''
def p_statement_1(p):
    '''statement : labeled_stmt'''
def p_statement_2(p):
    '''statement : simple_stmt'''
def p_statement_3(p):
    '''statement : go_stmt'''
def p_statement_4(p):
    '''statement : return_stmt'''
def p_statement_5(p):
    '''statement : break_stmt'''
def p_statement_6(p):
    '''statement : continue_stmt'''
def p_statement_7(p):
    '''statement : go_to_stmt'''
def p_statement_8(p):
    '''statement : block'''
def p_statement_9(p):
    '''statement : if_stmt M21'''
    backpatch(p[1]['nextlist'],p[2]['quad'])

def p_M21(p):
    '''M21 : empty'''
    p[0] = {}
    p[0]['quad'] = threeAddrCode.getNextInstNum()

def p_statement_10(p):
    '''statement : switch_stmt'''
def p_statement_11(p):
    '''statement : for_stmt'''


def p_simple_stmt(p):
    '''simple_stmt : expression_stmt
                    | inc_dec_stmt
                    | assignment
    '''

def p_label_stmt(p):
    '''labeled_stmt : label COLON statement
    '''

def p_label(p):
    '''label       : IDENTIFIER
    '''

def p_expression_stmt(p):
    '''expression_stmt : expression
    '''

def p_inc_dec_stmt(p):
    '''inc_dec_stmt : expression inc_dec_stmt_part '''

def p_inc_dec_stmt_part(p):
    '''inc_dec_stmt_part : PLUSPLUS
                        | MINUSMIN
    '''

def p_assignment(p):
    '''assignment : expression_list assign_op expression_list
    '''
    # print p[1]['value']+'='+str(p[3]['value'])
    # print p[1], p[3]
    if len(p[1])!= len(p[3]):
        print "Length of expression_list's are not same"
        p_error()
    else:
        for i in p[1]:
            check= symbol_table.locateSymbol(p[1][i]['value'])
            if (not check):
                print p[1][i]['value']+ 'not a variable'
                p_error(p)
            #Do type checking accordingly.
            if p[2]==None:
                threeAddrCode.emitCode(p[1][i]['value'],p[3][i]['value'],None, None)
                if (debug):
                    print(p[1][i]['value'],"=",p[3][i]['value'])
            else:
                threeAddrCode.emitCode(p[1][i]['value'],p[3][i]['value'],p[2],p[1][i]['value'])
                if debug :
                    print(p[1][i]['value'],p[3][i]['value'],p[2],p[1][i]['value'])

def p_assign_op(p):
    '''assign_op : assign_op_part EQUAL
    '''
    p[0]=p[1]

def p_assign_op_part(p):
    ''' assign_op_part : add_op
                        | mul_op
                        | empty
    '''
    p[0]=p[1]

def p_if_stmt(p):
    '''if_stmt : IF expression M22 block M23 M25 if_stmt_part2
    '''
    backpatch(p[2]['truelist'],p[3]['quad'])
    backpatch(p[2]['falselist'],p[6]['quad'])
    backpatch(p[4]['nextlist'],p[5]['quad'])
    merge(p[0]['nextlist'],p[5]['list'])

# def p_if_stmt_part1(p):
#     ''' if_stmt_part1 : simple_stmt SEMICOL
#                         | empty
#     '''

def p_if_stmt_part2(p):
    ''' if_stmt_part2 : ELSE if_stmt_part3
                        | empty
    '''

def p_if_stmt_part3(p):
    ''' if_stmt_part3 : if_stmt M24
                        | block  M24'''
    backpatch(p[1]['nextlist'],p[2]['quad'])

def p_switch_stmt(p):
    ''' switch_stmt : expr_switch_stmt '''

def p_M22(p):
    '''M22 : empty'''
    p[0] = {}
    p[0]['quad']=threeAddrCode.getNextInstNum()

def p_M23(p):
    '''M23 : empty'''
    p[0] = {}
    p[0]['quad']=threeAddrCode.getNextInstNum()
    p[0]['list']=[threeAddrCode.getNextInstNum()]
    threeAddrCode.emitCode("goto",None,None,None)

def p_M24(p):
    '''M24 : empty'''
    p[0] = {}
    p[0]['quad']=threeAddrCode.getNextInstNum()

def p_M25(p):
    '''M25 : empty'''
    p[0] = {}
    p[0]['quad']=threeAddrCode.getNextInstNum()

def p_expr_switch_stmt(p):
    '''expr_switch_stmt : SWITCH expr_switch_stmt_part1 LBRACE expr_switch_stmt_part2 RBRACE
    '''
    # '''expr_switch_stmt : SWITCH if_stmt_part1 expr_switch_stmt_part1 LBRACE expr_switch_stmt_part2 RBRACE
    # '''

def p_expr_switch_stmt_part1(p):
    '''expr_switch_stmt_part1 : expression
                            | empty
    '''

def p_expr_switch_stmt_part2(p):
    '''expr_switch_stmt_part2 : expr_case_clause expr_switch_stmt_part2
                                | empty
    '''

def p_expr_case_clause(p):
    '''expr_case_clause : expr_switch_case COLON statement_list
    '''

def p_expr_switch_case(p):
    '''expr_switch_case : CASE expression_list
                            | DEFAULT
    '''

def p_for_stmt(p):
    '''for_stmt : FOR for_stmt_part block
    '''

def p_for_stmt_part(p):
    ''' for_stmt_part : condition
                        | for_clause
    '''


def p_for_clause(p):
    '''for_clause :  init_stmt  SEMICOL  condition  SEMICOL  post_stmt
    '''

def p_init_stmt(p):
    '''init_stmt : simple_stmt
    '''

def p_post_stmt(p):
    '''post_stmt : simple_stmt
    '''
def p_condition(p):
    '''condition : expression
                | empty
    '''

# def p_range_clause(p):
#     '''range_clause : range_clause_part RANGE expression
#     '''
#
# def p_range_clause_part(p):
#     ''' range_clause_part : expression_list EQUAL
#                             | identifier_list COLONEQ
#                             | empty
#     '''

def p_go_stmt(p):
    '''go_stmt : GO expression
    '''
def p_go_to_stmt(p):
    '''go_to_stmt : GOTO IDENTIFIER
    '''

def p_return_stmt(p):
    '''return_stmt : RETURN expr_switch_stmt_part1'''

def p_break_stmt(p):
    '''break_stmt : BREAK break_stmt_part1
    '''

def p_break_stmt_part1(p):
    ''' break_stmt_part1 : label
                        | empty
    '''

def p_continue_stmt(p):
    '''continue_stmt : CONTINUE break_stmt_part1
    '''

########################################
############# ERROR ####################
########################################
def p_error(p):
    print "line :",p.lineno,"-Parsing Error Found at Token:",p.type
    parser.errok()

old_stderr = sys.stderr


parser = yacc.yacc(start = 'start', debug = True)

##############################################################################
import sys,re
def make_ParseTree(file_name):
    f = open("intermediate_dot","r")
    f = f.read()
    f = re.findall(r'Reduce rule \[(.*?)\] with',f)
    f.reverse()
    productions = [f[i].split(' -> ') for i in range(len(f))]
    derv = productions[0][0]
    file_name = file_name.replace(".\\","")
    file_name = file_name.split(".")[0]+".html"
    htm = open(file_name,"w")
    for i in range(len(productions)):
        # print derv
        htm.write(derv+"<br><br>")
        derv=derv.replace("<empty>","")
        derv=derv.replace("<b>","")
        derv=derv.replace("</b>","")
        revdev = derv[::-1]
        #print revdev
        toberep = productions[i][0][::-1]
        #print toberep
        replacem = "<b>"+productions[i][1]+"</b>"
        replacem = replacem[::-1]
        #print replacem
        revdev = revdev.replace(toberep,replacem,1)
        #print revdev
        derv = revdev[::-1]
        # htm.write("Using Prooduction:        "+f[i]+"<br>")
        # print derv
    # print derv
    htm.write(derv+"<br>")
    print "Writing html"
    htm.close()


#Scanning the file name
if (len(sys.argv) == 1):
    file_name =raw_input( "Give an Go file to parse: ")
else:
    file_name = sys.argv[1]

try:
    lexer = lex.lex()

    with open(file_name) as fp:#opening file
        data = fp.read()
        parser = yacc.yacc(start = 'start', debug = True)


        lexer.input(data)
        sys.stderr = open("intermediate_dot" , 'w')
        result = parser.parse(data , debug = 1)
        sys.stderr = old_stderr
        make_ParseTree(file_name)

except IOError as e:
    print "I/O error({0}): "+ "We are not able to open " + file_name + " . Does it Exists? Check permissions!"
