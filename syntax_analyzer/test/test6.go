// `for` is Go's only looping construct. Here are
// three basic types of `for` loops.

package main

import "fmt"

func main() {

    // The most basic type, with a single condition.
    // i := 1
    // for i <= 3 {
    //     // fmt.Println(i)
    //     // i = i + 1
    // }
    // for j = 7; j <= 9; j++ {
    //     fmt.Println(j)
    // }
    var a * int
    // (*ipt) = 0
    //Currently Range clause is not employed
    //fmt.Printf()
    //fmt.Printf("%s\n", strings.Join("%s\n",b))
    // `for` without a condition will loop repeatedly
    // until you `break` out of the loop or `return` from
    // the enclosing function.
    // for {
    //     // fmt.Println("loop")
    //     break
    // }
}
