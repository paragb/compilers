
# parsetab.py
# This file is automatically generated. Do not edit.
_tabversion = '3.8'

_lr_method = 'LALR'

_lr_signature = '825D9377B3B81B35A1CF79C1B4B43905'
    
_lr_action_items = {'OROR':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,144,-64,144,-69,144,-68,144,-47,-49,-48,144,144,144,144,-120,-71,-62,144,-50,144,144,-45,-51,-53,144,]),'CARET':([62,63,64,65,67,68,69,70,73,74,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,129,-61,-66,-44,-46,145,-64,145,-69,145,-5,-68,145,-47,-49,-48,-28,-26,145,-5,145,145,-120,-71,-62,145,-50,145,145,-45,-51,-53,-27,145,]),'GREAT':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,150,-64,150,-69,150,-68,150,-47,-49,-48,150,150,150,150,-120,-71,-62,150,-50,150,150,-45,-51,-53,150,]),'SEMICOL':([9,11,13,22,26,30,36,40,42,43,44,47,49,50,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,94,95,96,97,98,99,101,102,106,107,108,109,111,112,113,114,117,119,120,121,123,124,127,138,140,141,167,171,172,173,174,177,178,179,180,185,186,188,190,191,198,199,200,201,202,203,204,205,206,207,208,209,211,217,218,220,221,222,224,227,228,229,231,232,233,234,239,246,247,248,250,253,257,258,259,],[-12,-10,-11,-18,-13,-22,-31,-32,-30,-19,-29,-14,-15,-23,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-5,-125,-131,-128,-137,-134,-127,-130,183,-136,-69,-129,-126,-138,-135,-124,-133,-5,-5,-155,-141,-132,-68,-47,-49,-48,219,-28,-26,-21,-16,-158,-174,-157,-173,-171,-168,228,-141,-172,-140,-176,-177,-175,-178,-120,-142,-143,-144,-33,-36,-71,-62,-50,-43,-41,-37,-5,-139,-164,-5,-5,-145,-45,-51,-53,-27,256,-170,-150,-152,-156,-153,-151,-154,]),'CONSTANT':([1,5,7,8,9,11,13,14,18,22,26,28,30,31,32,33,35,36,40,42,43,44,47,49,50,51,52,53,54,61,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,93,127,138,140,141,171,172,173,174,181,183,203,207,208,209,211,217,218,221,222,232,233,234,239,255,],[6,-9,6,-8,-12,-10,-11,6,-6,-18,-13,-7,-22,-114,-5,-115,-116,-31,-32,-30,-19,-29,-14,-15,-23,-102,-101,-103,-104,6,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-105,-68,-47,-49,-48,-28,-26,-21,-16,6,6,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,6,]),'AMPCAR':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,147,-64,147,-69,147,-5,-68,147,-47,-49,-48,-28,-26,147,-5,147,147,-120,-71,-62,147,-50,147,147,-45,-51,-53,-27,147,]),'LESS':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,148,-64,148,-69,148,-68,148,-47,-49,-48,148,148,148,148,-120,-71,-62,148,-50,148,148,-45,-51,-53,148,]),'EQEQ':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,149,-64,149,-69,149,-68,149,-47,-49,-48,149,149,149,149,-120,-71,-62,149,-50,149,149,-45,-51,-53,149,]),'EQUAL':([21,23,25,36,40,42,44,45,49,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,86,108,118,123,127,138,140,141,145,147,152,154,156,157,159,160,162,165,166,171,172,190,193,194,196,197,203,207,208,209,211,217,218,221,222,232,233,234,239,],[41,-24,48,-31,-32,-30,-29,85,88,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-25,-69,-5,-5,-68,-47,-49,-48,-93,-100,-91,-97,-90,-95,-94,-98,-99,-92,-96,-28,-26,-5,-148,230,-147,-149,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,]),'ELSE':([203,229,],[-120,249,]),'GO':([61,181,183,255,],[115,115,115,115,]),'COLON':([62,63,64,65,67,68,69,70,73,75,76,77,78,80,84,104,108,127,138,140,141,171,172,203,209,211,217,218,222,232,233,234,239,241,244,252,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-5,181,-140,-68,-47,-49,-48,-28,-26,-120,-71,-62,-50,-43,-5,-45,-51,-53,-27,-163,255,-162,]),'AMPAMP':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,151,-64,151,-69,151,-68,151,-47,-49,-48,151,151,151,151,-120,-71,-62,151,-50,151,151,-45,-51,-53,151,]),'RETURN':([61,181,183,255,],[94,94,94,94,]),'MINUS':([62,63,64,65,67,68,69,70,73,74,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,131,-61,-66,-44,-46,152,-64,152,-69,152,-5,-68,152,-47,-49,-48,-28,-26,152,-5,152,152,-120,-71,-62,152,-50,152,152,-45,-51,-53,-27,152,]),'DOT':([70,108,],[126,126,]),'GOTO':([61,181,183,255,],[100,100,100,100,]),'CASE':([103,183,225,226,243,260,],[-121,-123,240,-122,240,-161,]),'GEQ':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,153,-64,153,-69,153,-68,153,-47,-49,-48,153,153,153,153,-120,-71,-62,153,-50,153,153,-45,-51,-53,153,]),'RPAREN':([34,36,40,42,44,55,56,58,60,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,84,90,91,92,127,137,138,140,141,142,171,172,175,203,207,208,209,210,211,213,214,215,216,217,218,221,222,223,232,233,234,235,237,239,251,],[-5,-31,-32,-30,-29,-106,-5,93,-107,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-5,-110,-108,-111,-68,211,-47,-49,-48,-5,-28,-26,-5,-120,-33,-36,-71,232,-62,234,-5,-56,-54,-50,-43,-37,-5,-109,-45,-51,-53,-55,-58,-27,-57,]),'LL':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,154,-64,154,-69,154,-5,-68,154,-47,-49,-48,-28,-26,154,-5,154,154,-120,-71,-62,154,-50,154,154,-45,-51,-53,-27,154,]),'BASETYPE':([21,23,25,29,32,34,39,57,59,60,86,89,93,125,142,168,176,],[40,-24,40,40,40,-5,40,40,-112,-113,-25,-5,-105,40,40,40,-113,]),'PLUS':([62,63,64,65,67,68,69,70,73,74,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,132,-61,-66,-44,-46,156,-64,156,-69,156,-5,-68,156,-47,-49,-48,-28,-26,156,-5,156,156,-120,-71,-62,156,-50,156,156,-45,-51,-53,-27,156,]),'NOTEQ':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,155,-64,155,-69,155,-68,155,-47,-49,-48,155,155,155,155,-120,-71,-62,155,-50,155,155,-45,-51,-53,155,]),'SWITCH':([61,181,183,255,],[105,105,105,105,]),'COMMA':([23,36,40,42,44,56,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,84,92,108,123,127,138,140,141,175,190,203,207,208,209,211,214,217,218,221,222,232,233,234,],[46,-31,-32,-30,-29,89,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,170,-111,-69,170,-68,-47,-49,-48,89,170,-120,-33,-36,-71,-62,236,-50,-43,-37,170,-45,-51,-53,]),'INTEGER':([37,41,48,61,74,85,88,94,105,110,115,116,129,130,131,132,133,134,135,136,139,142,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,181,183,195,228,230,236,240,255,256,],[69,69,69,69,69,69,69,69,69,69,69,69,-74,69,-73,-72,-78,-76,-75,-77,69,69,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,69,-94,-98,-82,-99,-87,-81,-92,-96,69,69,69,69,69,-146,69,69,69,69,]),'IDENTIFIER':([3,4,6,10,16,34,37,41,46,48,61,74,81,85,88,89,94,100,105,110,115,116,119,120,126,129,130,131,132,133,134,135,136,139,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,181,183,195,219,228,230,236,240,255,256,],[18,19,23,23,29,23,70,70,23,70,108,70,23,70,70,23,70,180,70,70,70,70,198,198,209,-74,70,-73,-72,-78,-76,-75,-77,70,70,217,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,70,-94,-98,-82,-99,-87,-81,-92,-96,70,108,108,70,23,70,-146,70,70,108,70,]),'RBRACE':([103,122,169,183,219,225,226,238,242,243,245,254,260,],[-121,203,221,-123,-39,-5,-122,-38,253,-5,-160,-159,-161,]),'$end':([1,2,5,7,8,9,11,12,13,14,17,18,22,24,26,27,28,30,31,32,33,35,36,40,42,43,44,47,49,50,51,52,53,54,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,93,127,138,140,141,171,172,173,174,203,207,208,209,211,217,218,221,222,232,233,234,239,],[-5,0,-9,-5,-8,-12,-10,-1,-11,-5,-4,-6,-18,-2,-13,-3,-7,-22,-114,-5,-115,-116,-31,-32,-30,-19,-29,-14,-15,-23,-102,-101,-103,-104,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-105,-68,-47,-49,-48,-28,-26,-21,-16,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,]),'FUNCTION':([1,5,7,8,9,11,13,14,18,22,26,28,30,31,32,33,35,36,37,40,41,42,43,44,47,48,49,50,51,52,53,54,61,62,63,64,65,67,68,69,70,73,74,75,76,77,78,80,82,83,84,85,87,88,93,94,105,110,115,116,127,129,130,131,132,133,134,135,136,138,139,140,141,142,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,171,172,173,174,181,183,195,203,207,208,209,211,217,218,221,222,228,230,232,233,234,236,239,240,255,256,],[4,-9,4,-8,-12,-10,-11,4,-6,-18,-13,-7,-22,-114,-5,-115,-116,-31,72,-32,72,-30,-19,-29,-14,72,-15,-23,-102,-101,-103,-104,72,-118,-119,-70,-42,-63,-60,-65,-69,-67,72,-61,-66,-44,-46,-64,-40,-20,-5,72,-17,72,-105,72,72,72,72,72,-68,-74,72,-73,-72,-78,-76,-75,-77,-47,72,-49,-48,72,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,72,-94,-98,-82,-99,-87,-81,-92,-96,72,-28,-26,-21,-16,72,72,72,-120,-33,-36,-71,-62,-50,-43,-37,-5,72,-146,-45,-51,-53,72,-27,72,72,72,]),'LMINUS':([74,],[133,]),'STRING':([15,37,41,48,61,74,85,88,94,105,110,115,116,129,130,131,132,133,134,135,136,139,142,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,181,183,195,228,230,236,240,255,256,],[28,73,73,73,73,73,73,73,73,73,73,73,73,-74,73,-73,-72,-78,-76,-75,-77,73,73,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,73,-94,-98,-82,-99,-87,-81,-92,-96,73,73,73,73,73,-146,73,73,73,73,]),'FOR':([61,181,183,255,],[110,110,110,110,]),'PACKAGE':([0,],[3,]),'PLUSPLUS':([62,63,64,65,67,68,69,70,73,75,76,77,78,80,108,123,127,138,140,141,190,203,209,211,217,218,232,233,234,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-69,205,-68,-47,-49,-48,205,-120,-71,-62,-50,-43,-45,-51,-53,]),'RBRACK':([37,62,63,64,65,66,67,68,69,70,71,73,75,76,77,78,79,80,127,138,140,141,203,209,211,212,217,218,232,233,234,],[-5,-118,-119,-70,-42,125,-63,-60,-65,-69,-35,-67,-61,-66,-44,-46,-34,-64,-68,-47,-49,-48,-120,-71,-62,233,-50,-43,-45,-51,-53,]),'DIVIDE':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,157,-64,157,-69,157,-5,-68,157,-47,-49,-48,-28,-26,157,-5,157,157,-120,-71,-62,157,-50,157,157,-45,-51,-53,-27,157,]),'TIMES':([21,23,25,29,32,34,39,57,59,60,62,63,64,65,67,68,69,70,73,74,75,76,77,78,79,80,84,86,89,93,108,118,123,125,127,137,138,140,141,142,168,171,172,176,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[39,-24,39,39,39,-5,39,39,-112,-113,-118,-119,-70,-42,-63,-60,-65,-69,-67,134,-61,-66,-44,-46,159,-64,159,-25,-5,-105,-69,159,-5,39,-68,159,-47,-49,-48,39,39,-28,-26,-113,159,-5,159,159,-120,-71,-62,159,-50,159,159,-45,-51,-53,-27,159,]),'GG':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,160,-64,160,-69,160,-5,-68,160,-47,-49,-48,-28,-26,160,-5,160,160,-120,-71,-62,160,-50,160,160,-45,-51,-53,-27,160,]),'MINUSMIN':([62,63,64,65,67,68,69,70,73,75,76,77,78,80,108,123,127,138,140,141,190,203,209,211,217,218,232,233,234,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-69,206,-68,-47,-49,-48,206,-120,-71,-62,-50,-43,-45,-51,-53,]),'LPAREN':([19,20,32,37,41,48,61,62,63,64,67,68,69,70,72,73,74,75,76,77,78,80,85,88,93,94,105,108,110,115,116,127,129,130,131,132,133,134,135,136,138,139,140,141,142,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,181,183,195,203,209,211,217,228,230,233,234,236,240,255,256,],[-117,34,34,74,74,74,74,-118,-119,-70,-63,-60,-65,-69,34,-67,74,-61,-66,142,-46,-64,74,74,-105,74,74,-69,74,74,74,-68,-74,74,-73,-72,-78,-76,-75,-77,-47,74,-49,-48,74,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,74,-94,-98,-82,-99,-87,-81,-92,-96,74,74,74,74,-120,-71,-62,-50,74,-146,-51,-53,74,74,74,74,]),'IMPORT':([1,5,7,8,9,11,13,14,18,22,26,28,30,31,32,33,35,36,40,42,43,44,47,49,50,51,52,53,54,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,93,127,138,140,141,171,172,173,174,203,207,208,209,211,217,218,221,222,232,233,234,239,],[15,-9,15,-8,-12,-10,-11,15,-6,-18,-13,-7,-22,-114,-5,-115,-116,-31,-32,-30,-19,-29,-14,-15,-23,-102,-101,-103,-104,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-105,-68,-47,-49,-48,-28,-26,-21,-16,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,]),'VAR':([1,5,7,8,9,11,13,14,18,22,26,28,30,31,32,33,35,36,40,42,43,44,47,49,50,51,52,53,54,61,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,93,127,138,140,141,171,172,173,174,181,183,203,207,208,209,211,217,218,221,222,232,233,234,239,255,],[10,-9,10,-8,-12,-10,-11,10,-6,-18,-13,-7,-22,-114,-5,-115,-116,-31,-32,-30,-19,-29,-14,-15,-23,-102,-101,-103,-104,10,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-105,-68,-47,-49,-48,-28,-26,-21,-16,10,10,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,10,]),'MING':([62,63,64,67,68,69,70,73,75,76,77,78,80,108,127,138,140,141,203,209,211,217,233,234,],[-118,-119,-70,-63,-60,-65,-69,-67,-61,-66,143,-46,-64,-69,-68,-47,-49,-48,-120,-71,-62,-50,-51,-53,]),'IF':([61,181,183,249,255,],[116,116,116,116,116,]),'LBRACE':([32,35,36,38,40,42,44,51,52,53,54,61,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,84,93,98,105,107,110,112,123,127,128,138,140,141,171,172,177,179,181,182,183,184,185,187,189,190,192,203,204,205,206,207,208,209,211,217,218,221,222,231,232,233,234,239,249,255,261,262,],[-5,61,-31,81,-32,-30,-29,-102,-101,-103,-104,61,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-5,-105,-137,-5,-136,-5,-138,-141,-68,61,-47,-49,-48,-28,-26,-158,-157,61,225,61,61,-171,-165,-166,-170,61,-120,-142,-143,-144,-33,-36,-71,-62,-50,-43,-37,-5,-145,-45,-51,-53,-27,61,61,-169,-167,]),'STRUCT':([21,23,25,29,32,34,39,57,59,60,86,89,93,125,142,168,176,],[38,-24,38,38,38,-5,38,38,-112,-113,-25,-5,-105,38,38,38,-113,]),'FLOAT':([37,41,48,61,74,85,88,94,105,110,115,116,129,130,131,132,133,134,135,136,139,142,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,170,181,183,195,228,230,236,240,255,256,],[76,76,76,76,76,76,76,76,76,76,76,76,-74,76,-73,-72,-78,-76,-75,-77,76,76,-80,-93,-83,-100,-86,-84,-88,-79,-91,-89,-97,-85,-90,-95,76,-94,-98,-82,-99,-87,-81,-92,-96,76,76,76,76,76,-146,76,76,76,76,]),'AMPERS':([62,63,64,65,67,68,69,70,73,74,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,136,-61,-66,-44,-46,162,-64,162,-69,162,-5,-68,162,-47,-49,-48,-28,-26,162,-5,162,162,-120,-71,-62,162,-50,162,162,-45,-51,-53,-27,162,]),'LBRACK':([21,23,25,29,32,34,39,57,59,60,62,63,64,67,68,69,70,73,75,76,77,78,80,86,89,93,108,125,127,138,140,141,142,168,176,203,209,211,217,233,234,],[37,-24,37,37,37,-5,37,37,-112,-113,-118,-119,-70,-63,-60,-65,-69,-67,-61,-66,139,-46,-64,-25,-5,-105,-69,37,-68,-47,-49,-48,37,37,-113,-120,-71,-62,-50,-51,-53,]),'DEFAULT':([103,183,225,226,243,260,],[-121,-123,241,-122,241,-161,]),'BREAK':([61,181,183,255,],[119,119,119,119,]),'LEQ':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,123,127,137,138,140,141,179,190,191,192,203,209,211,212,217,218,222,232,233,234,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,163,-64,163,-69,163,-68,163,-47,-49,-48,163,163,163,163,-120,-71,-62,163,-50,163,163,-45,-51,-53,163,]),'CONTINUE':([61,181,183,255,],[120,120,120,120,]),'NOT':([74,],[135,]),'TYPE':([1,5,7,8,9,11,13,14,18,22,26,28,30,31,32,33,35,36,40,42,43,44,47,49,50,51,52,53,54,61,62,63,64,65,67,68,69,70,73,75,76,77,78,80,82,83,84,87,93,127,138,140,141,171,172,173,174,181,183,203,207,208,209,211,217,218,221,222,232,233,234,239,255,],[16,-9,16,-8,-12,-10,-11,16,-6,-18,-13,-7,-22,-114,-5,-115,-116,-31,-32,-30,-19,-29,-14,-15,-23,-102,-101,-103,-104,16,-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,-64,-40,-20,-5,-17,-105,-68,-47,-49,-48,-28,-26,-21,-16,16,16,-120,-33,-36,-71,-62,-50,-43,-37,-5,-45,-51,-53,-27,16,]),'OR':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,165,-64,165,-69,165,-5,-68,165,-47,-49,-48,-28,-26,165,-5,165,165,-120,-71,-62,165,-50,165,165,-45,-51,-53,-27,165,]),'MOD':([62,63,64,65,67,68,69,70,73,75,76,77,78,79,80,84,108,118,123,127,137,138,140,141,171,172,179,190,191,192,203,209,211,212,217,218,222,232,233,234,239,247,],[-118,-119,-70,-42,-63,-60,-65,-69,-67,-61,-66,-44,-46,166,-64,166,-69,166,-5,-68,166,-47,-49,-48,-28,-26,166,-5,166,166,-120,-71,-62,166,-50,166,166,-45,-51,-53,-27,166,]),}

_lr_action = {}
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = {}
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'qualified_ident':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,]),'parameter_decl':([34,89,],[56,175,]),'declaration':([1,7,14,61,181,183,255,],[8,8,8,114,114,114,114,]),'parameters':([20,32,72,],[32,51,32,]),'for_stmt_part':([110,],[184,]),'parameter_decl_part':([34,89,],[57,57,]),'expr_switch_case':([225,243,],[244,244,]),'simple_stmt':([61,110,181,183,255,256,],[111,186,111,111,111,261,]),'binary_op':([79,84,123,137,179,190,191,192,212,218,222,247,],[158,158,158,158,158,158,158,158,158,158,158,158,]),'expr_switch_stmt_part2':([225,243,],[242,254,]),'for_stmt':([61,181,183,255,],[113,113,113,113,]),'type_decl':([1,7,14,61,181,183,255,],[13,13,13,13,13,13,13,]),'condition':([110,228,],[187,246,]),'comma_expression':([84,123,190,222,],[172,172,172,239,]),'if_stmt':([61,181,183,249,255,],[117,117,117,257,117,]),'parameter_list':([34,],[55,]),'var_spec_part':([25,],[47,]),'break_stmt_part1':([119,120,],[201,202,]),'primary_expr':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,77,]),'Operand':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,78,]),'rel_op':([79,84,123,137,179,190,191,192,212,218,222,247,],[164,164,164,164,164,164,164,164,164,164,164,164,]),'function_lit':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,]),'switch_stmt':([61,181,183,255,],[99,99,99,99,]),'array_length':([37,],[66,]),'assign_op_part':([118,],[194,]),'parameter_list_part':([56,175,],[91,223,]),'result':([32,],[52,]),'struct_type_part':([81,219,],[169,238,]),'index':([77,],[138,]),'pointer_type':([21,25,29,32,39,57,125,142,168,],[36,36,36,36,36,36,36,36,36,]),'label':([61,119,120,181,183,255,],[104,199,199,104,104,104,]),'type_spec':([16,],[30,]),'if_stmt_part3':([249,],[258,]),'if_stmt_part2':([229,],[248,]),'expr_case_clause':([225,243,],[243,243,]),'assign_op':([118,],[195,]),'array_type':([21,25,29,32,39,57,125,142,168,],[44,44,44,44,44,44,44,44,44,]),'operand_name':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,]),'func_decl_part':([20,],[31,]),'var_decl':([1,7,14,61,181,183,255,],[9,9,9,9,9,9,9,]),'const_decl':([1,7,14,61,181,183,255,],[11,11,11,11,11,11,11,]),'element_type':([125,],[207,]),'expr_switch_stmt':([61,181,183,255,],[121,121,121,121,]),'signature':([20,72,],[35,128,]),'statement_list':([61,255,],[122,260,]),'post_stmt':([256,],[262,]),'expression':([37,41,48,61,74,85,88,94,105,110,115,116,139,142,158,170,181,183,195,228,236,240,255,256,],[79,84,84,123,137,84,84,179,179,190,191,192,212,84,218,222,123,123,84,247,84,84,123,123,]),'block':([35,61,128,181,183,184,192,249,255,],[63,124,63,124,124,227,229,259,124,]),'mul_op':([79,84,118,123,137,179,190,191,192,212,218,222,247,],[146,146,193,146,146,146,146,146,146,146,146,146,146,]),'labeled_stmt':([61,181,183,255,],[95,95,95,95,]),'unary_expr':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[65,65,65,65,65,65,65,65,65,65,65,65,210,65,65,65,65,65,65,65,65,65,65,65,65,]),'go_to_stmt':([61,181,183,255,],[96,96,96,96,]),'statement_list_part':([61,183,255,],[103,226,103,]),'return_stmt':([61,181,183,255,],[97,97,97,97,]),'arguments_part':([142,],[213,]),'function_body':([35,128,],[62,62,]),'go_stmt':([61,181,183,255,],[101,101,101,101,]),'continue_stmt':([61,181,183,255,],[102,102,102,102,]),'struct_type':([21,25,29,32,39,57,125,142,168,],[42,42,42,42,42,42,42,42,42,]),'start':([0,],[2,]),'literal':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,68,]),'program':([1,7,14,],[12,24,27,]),'statement':([61,181,183,255,],[106,224,106,106,]),'type':([21,25,29,32,39,57,125,142,168,],[45,49,50,53,82,92,208,214,220,]),'empty':([1,7,14,32,34,37,56,84,89,94,105,110,118,119,120,123,142,175,190,214,222,225,228,229,243,],[17,17,17,54,60,71,90,171,176,177,177,185,197,200,200,171,215,90,171,237,171,245,185,250,245,]),'function_name':([4,],[20,]),'function':([20,72,],[33,127,]),'selector':([77,],[140,]),'add_op':([79,84,118,123,137,179,190,191,192,212,218,222,247,],[161,161,196,161,161,161,161,161,161,161,161,161,161,]),'parameter_part':([34,],[58,]),'inc_dec_stmt':([61,110,181,183,255,256,],[98,98,98,98,98,98,]),'unary_op':([74,],[130,]),'imports':([1,7,14,],[7,7,7,]),'basic_lit':([37,41,48,61,74,85,88,94,105,110,115,116,130,139,142,158,170,181,183,195,228,236,240,255,256,],[67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,]),'break_stmt':([61,181,183,255,],[109,109,109,109,]),'init_stmt':([110,],[188,]),'const_spec_part':([21,],[43,]),'expression_stmt':([61,110,181,183,255,256,],[107,107,107,107,107,107,]),'arguments':([77,],[141,]),'const_spec':([6,],[22,]),'pkg_definition':([0,],[1,]),'inc_dec_stmt_part':([123,190,],[204,204,]),'assignment':([61,110,181,183,255,256,],[112,112,112,112,112,112,]),'top_level_decl':([1,7,14,],[14,14,14,]),'arguments_part_two':([214,],[235,]),'identifier_list':([6,10,34,46,81,89,219,],[21,25,59,86,168,59,168,]),'function_decl':([1,7,14,],[5,5,5,]),'field_decl':([81,219,],[167,167,]),'expression_list':([41,48,61,85,88,110,142,181,183,195,236,240,255,256,],[83,87,118,173,174,118,216,118,118,231,251,252,118,118,]),'expr_switch_stmt_part1':([94,105,],[178,182,]),'for_clause':([110,],[189,]),'var_spec':([10,],[26,]),}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
   for _x, _y in zip(_v[0], _v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = {}
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> start","S'",1,None,None,None),
  ('start -> pkg_definition program','start',2,'p_start','parser.py',22),
  ('program -> imports program','program',2,'p_program','parser.py',26),
  ('program -> top_level_decl program','program',2,'p_program','parser.py',27),
  ('program -> empty','program',1,'p_program','parser.py',28),
  ('empty -> <empty>','empty',0,'p_empty','parser.py',32),
  ('pkg_definition -> PACKAGE IDENTIFIER','pkg_definition',2,'p_pkg_definition','parser.py',36),
  ('imports -> IMPORT STRING','imports',2,'p_imports','parser.py',40),
  ('top_level_decl -> declaration','top_level_decl',1,'p_top_level_decl','parser.py',44),
  ('top_level_decl -> function_decl','top_level_decl',1,'p_top_level_decl','parser.py',45),
  ('declaration -> const_decl','declaration',1,'p_declaration','parser.py',49),
  ('declaration -> type_decl','declaration',1,'p_declaration','parser.py',50),
  ('declaration -> var_decl','declaration',1,'p_declaration','parser.py',51),
  ('var_decl -> VAR var_spec','var_decl',2,'p_var_decl','parser.py',55),
  ('var_spec -> identifier_list var_spec_part','var_spec',2,'p_var_spec','parser.py',59),
  ('var_spec_part -> type','var_spec_part',1,'p_var_spec_part','parser.py',62),
  ('var_spec_part -> type EQUAL expression_list','var_spec_part',3,'p_var_spec_part','parser.py',63),
  ('var_spec_part -> EQUAL expression_list','var_spec_part',2,'p_var_spec_part','parser.py',64),
  ('const_decl -> CONSTANT const_spec','const_decl',2,'p_const_decl','parser.py',68),
  ('const_spec -> identifier_list const_spec_part','const_spec',2,'p_const_spec','parser.py',72),
  ('const_spec_part -> EQUAL expression_list','const_spec_part',2,'p_const_spec_part','parser.py',76),
  ('const_spec_part -> type EQUAL expression_list','const_spec_part',3,'p_const_spec_part','parser.py',77),
  ('type_decl -> TYPE type_spec','type_decl',2,'p_type_decl','parser.py',80),
  ('type_spec -> IDENTIFIER type','type_spec',2,'p_type_spec','parser.py',84),
  ('identifier_list -> IDENTIFIER','identifier_list',1,'p_identifier_list','parser.py',88),
  ('identifier_list -> IDENTIFIER COMMA identifier_list','identifier_list',3,'p_identifier_list','parser.py',89),
  ('expression_list -> expression comma_expression','expression_list',2,'p_expression_list','parser.py',98),
  ('comma_expression -> COMMA expression comma_expression','comma_expression',3,'p_comma_expression','parser.py',102),
  ('comma_expression -> empty','comma_expression',1,'p_comma_expression','parser.py',103),
  ('type -> array_type','type',1,'p_type','parser.py',107),
  ('type -> struct_type','type',1,'p_type','parser.py',108),
  ('type -> pointer_type','type',1,'p_type','parser.py',109),
  ('type -> BASETYPE','type',1,'p_type','parser.py',110),
  ('array_type -> LBRACK array_length RBRACK element_type','array_type',4,'p_array_type','parser.py',114),
  ('array_length -> expression','array_length',1,'p_array_length','parser.py',118),
  ('array_length -> empty','array_length',1,'p_array_length','parser.py',119),
  ('element_type -> type','element_type',1,'p_element_type','parser.py',123),
  ('struct_type -> STRUCT LBRACE struct_type_part RBRACE','struct_type',4,'p_struct_type','parser.py',127),
  ('struct_type_part -> field_decl SEMICOL struct_type_part','struct_type_part',3,'p_struct_type_part','parser.py',131),
  ('struct_type_part -> field_decl SEMICOL','struct_type_part',2,'p_struct_type_part','parser.py',132),
  ('pointer_type -> TIMES type','pointer_type',2,'p_pointer_type','parser.py',136),
  ('field_decl -> identifier_list type','field_decl',2,'p_field_decl','parser.py',140),
  ('expression -> unary_expr','expression',1,'p_expression','parser.py',144),
  ('expression -> expression binary_op expression','expression',3,'p_expression','parser.py',145),
  ('unary_expr -> primary_expr','unary_expr',1,'p_unary_expr','parser.py',148),
  ('unary_expr -> LPAREN unary_op unary_expr RPAREN','unary_expr',4,'p_unary_expr','parser.py',149),
  ('primary_expr -> Operand','primary_expr',1,'p_primary_expr','parser.py',152),
  ('primary_expr -> primary_expr index','primary_expr',2,'p_primary_expr','parser.py',153),
  ('primary_expr -> primary_expr arguments','primary_expr',2,'p_primary_expr','parser.py',154),
  ('primary_expr -> primary_expr selector','primary_expr',2,'p_primary_expr','parser.py',155),
  ('selector -> MING IDENTIFIER','selector',2,'p_selector','parser.py',159),
  ('index -> LBRACK expression RBRACK','index',3,'p_index','parser.py',163),
  ('type_assertion -> DOT LPAREN type RPAREN','type_assertion',4,'p_type_assertion','parser.py',168),
  ('arguments -> LPAREN arguments_part RPAREN','arguments',3,'p_arguments','parser.py',171),
  ('arguments_part -> expression_list','arguments_part',1,'p_arguments_part','parser.py',174),
  ('arguments_part -> type arguments_part_two','arguments_part',2,'p_arguments_part','parser.py',175),
  ('arguments_part -> empty','arguments_part',1,'p_arguments_part','parser.py',176),
  ('arguments_part_two -> COMMA expression_list','arguments_part_two',2,'p_arguments_part_two','parser.py',179),
  ('arguments_part_two -> empty','arguments_part_two',1,'p_arguments_part_two','parser.py',180),
  ('conversion -> type LPAREN expression RPAREN','conversion',4,'p_conversion','parser.py',183),
  ('Operand -> literal','Operand',1,'p_operand','parser.py',187),
  ('Operand -> operand_name','Operand',1,'p_operand','parser.py',188),
  ('Operand -> LPAREN expression RPAREN','Operand',3,'p_operand','parser.py',189),
  ('literal -> basic_lit','literal',1,'p_literal','parser.py',193),
  ('literal -> function_lit','literal',1,'p_literal','parser.py',194),
  ('basic_lit -> INTEGER','basic_lit',1,'p_basic_lit','parser.py',198),
  ('basic_lit -> FLOAT','basic_lit',1,'p_basic_lit','parser.py',199),
  ('basic_lit -> STRING','basic_lit',1,'p_basic_lit','parser.py',200),
  ('function_lit -> FUNCTION function','function_lit',2,'p_func_lit','parser.py',203),
  ('operand_name -> IDENTIFIER','operand_name',1,'p_operand_name','parser.py',207),
  ('operand_name -> qualified_ident','operand_name',1,'p_operand_name','parser.py',208),
  ('qualified_ident -> IDENTIFIER DOT IDENTIFIER','qualified_ident',3,'p_qualified_ident','parser.py',211),
  ('unary_op -> PLUS','unary_op',1,'p_unary_op','parser.py',214),
  ('unary_op -> MINUS','unary_op',1,'p_unary_op','parser.py',215),
  ('unary_op -> CARET','unary_op',1,'p_unary_op','parser.py',216),
  ('unary_op -> NOT','unary_op',1,'p_unary_op','parser.py',217),
  ('unary_op -> TIMES','unary_op',1,'p_unary_op','parser.py',218),
  ('unary_op -> AMPERS','unary_op',1,'p_unary_op','parser.py',219),
  ('unary_op -> LMINUS','unary_op',1,'p_unary_op','parser.py',220),
  ('binary_op -> AMPAMP','binary_op',1,'p_binary_op','parser.py',224),
  ('binary_op -> OROR','binary_op',1,'p_binary_op','parser.py',225),
  ('binary_op -> rel_op','binary_op',1,'p_binary_op','parser.py',226),
  ('binary_op -> add_op','binary_op',1,'p_binary_op','parser.py',227),
  ('binary_op -> mul_op','binary_op',1,'p_binary_op','parser.py',228),
  ('rel_op -> EQEQ','rel_op',1,'p_rel_op','parser.py',232),
  ('rel_op -> NOTEQ','rel_op',1,'p_rel_op','parser.py',233),
  ('rel_op -> LESS','rel_op',1,'p_rel_op','parser.py',234),
  ('rel_op -> LEQ','rel_op',1,'p_rel_op','parser.py',235),
  ('rel_op -> GREAT','rel_op',1,'p_rel_op','parser.py',236),
  ('rel_op -> GEQ','rel_op',1,'p_rel_op','parser.py',237),
  ('add_op -> PLUS','add_op',1,'p_add_op','parser.py',241),
  ('add_op -> MINUS','add_op',1,'p_add_op','parser.py',242),
  ('add_op -> OR','add_op',1,'p_add_op','parser.py',243),
  ('add_op -> CARET','add_op',1,'p_add_op','parser.py',244),
  ('mul_op -> TIMES','mul_op',1,'p_mul_op','parser.py',248),
  ('mul_op -> DIVIDE','mul_op',1,'p_mul_op','parser.py',249),
  ('mul_op -> MOD','mul_op',1,'p_mul_op','parser.py',250),
  ('mul_op -> LL','mul_op',1,'p_mul_op','parser.py',251),
  ('mul_op -> GG','mul_op',1,'p_mul_op','parser.py',252),
  ('mul_op -> AMPERS','mul_op',1,'p_mul_op','parser.py',253),
  ('mul_op -> AMPCAR','mul_op',1,'p_mul_op','parser.py',254),
  ('signature -> parameters result','signature',2,'p_signature','parser.py',258),
  ('result -> parameters','result',1,'p_result','parser.py',261),
  ('result -> type','result',1,'p_result','parser.py',262),
  ('result -> empty','result',1,'p_result','parser.py',263),
  ('parameters -> LPAREN parameter_part RPAREN','parameters',3,'p_parameter','parser.py',267),
  ('parameter_part -> parameter_list','parameter_part',1,'p_parameter_part','parser.py',271),
  ('parameter_part -> empty','parameter_part',1,'p_parameter_part','parser.py',272),
  ('parameter_list -> parameter_decl parameter_list_part','parameter_list',2,'p_parameter_list','parser.py',276),
  ('parameter_list_part -> COMMA parameter_decl parameter_list_part','parameter_list_part',3,'p_parameter_list_part','parser.py',281),
  ('parameter_list_part -> empty','parameter_list_part',1,'p_parameter_list_part','parser.py',282),
  ('parameter_decl -> parameter_decl_part type','parameter_decl',2,'p_parameter_decl','parser.py',286),
  ('parameter_decl_part -> identifier_list','parameter_decl_part',1,'p_parameter_decl_part','parser.py',290),
  ('parameter_decl_part -> empty','parameter_decl_part',1,'p_parameter_decl_part','parser.py',291),
  ('function_decl -> FUNCTION function_name func_decl_part','function_decl',3,'p_func_decl','parser.py',295),
  ('func_decl_part -> function','func_decl_part',1,'p_func_decl_part','parser.py',299),
  ('func_decl_part -> signature','func_decl_part',1,'p_func_decl_part','parser.py',300),
  ('function_name -> IDENTIFIER','function_name',1,'p_func_name','parser.py',304),
  ('function -> signature function_body','function',2,'p_function','parser.py',308),
  ('function_body -> block','function_body',1,'p_function_body','parser.py',312),
  ('block -> LBRACE statement_list RBRACE','block',3,'p_block','parser.py',316),
  ('statement_list -> statement_list_part','statement_list',1,'p_statement_list','parser.py',320),
  ('statement_list_part -> statement SEMICOL statement_list_part','statement_list_part',3,'p_statement_list_part','parser.py',324),
  ('statement_list_part -> statement SEMICOL','statement_list_part',2,'p_statement_list_part','parser.py',325),
  ('statement -> declaration','statement',1,'p_statement','parser.py',329),
  ('statement -> labeled_stmt','statement',1,'p_statement','parser.py',330),
  ('statement -> simple_stmt','statement',1,'p_statement','parser.py',331),
  ('statement -> go_stmt','statement',1,'p_statement','parser.py',332),
  ('statement -> return_stmt','statement',1,'p_statement','parser.py',333),
  ('statement -> break_stmt','statement',1,'p_statement','parser.py',334),
  ('statement -> continue_stmt','statement',1,'p_statement','parser.py',335),
  ('statement -> go_to_stmt','statement',1,'p_statement','parser.py',336),
  ('statement -> block','statement',1,'p_statement','parser.py',337),
  ('statement -> if_stmt','statement',1,'p_statement','parser.py',338),
  ('statement -> switch_stmt','statement',1,'p_statement','parser.py',339),
  ('statement -> for_stmt','statement',1,'p_statement','parser.py',340),
  ('simple_stmt -> expression_stmt','simple_stmt',1,'p_simple_stmt','parser.py',344),
  ('simple_stmt -> inc_dec_stmt','simple_stmt',1,'p_simple_stmt','parser.py',345),
  ('simple_stmt -> assignment','simple_stmt',1,'p_simple_stmt','parser.py',346),
  ('labeled_stmt -> label COLON statement','labeled_stmt',3,'p_label_stmt','parser.py',350),
  ('label -> IDENTIFIER','label',1,'p_label','parser.py',354),
  ('expression_stmt -> expression','expression_stmt',1,'p_expression_stmt','parser.py',358),
  ('inc_dec_stmt -> expression inc_dec_stmt_part','inc_dec_stmt',2,'p_inc_dec_stmt','parser.py',362),
  ('inc_dec_stmt_part -> PLUSPLUS','inc_dec_stmt_part',1,'p_inc_dec_stmt_part','parser.py',365),
  ('inc_dec_stmt_part -> MINUSMIN','inc_dec_stmt_part',1,'p_inc_dec_stmt_part','parser.py',366),
  ('assignment -> expression_list assign_op expression_list','assignment',3,'p_assignment','parser.py',369),
  ('assign_op -> assign_op_part EQUAL','assign_op',2,'p_assign_op','parser.py',373),
  ('assign_op_part -> add_op','assign_op_part',1,'p_assign_op_part','parser.py',377),
  ('assign_op_part -> mul_op','assign_op_part',1,'p_assign_op_part','parser.py',378),
  ('assign_op_part -> empty','assign_op_part',1,'p_assign_op_part','parser.py',379),
  ('if_stmt -> IF expression block if_stmt_part2','if_stmt',4,'p_if_stmt','parser.py',383),
  ('if_stmt_part2 -> ELSE if_stmt_part3','if_stmt_part2',2,'p_if_stmt_part2','parser.py',392),
  ('if_stmt_part2 -> empty','if_stmt_part2',1,'p_if_stmt_part2','parser.py',393),
  ('if_stmt_part3 -> if_stmt','if_stmt_part3',1,'p_if_stmt_part3','parser.py',397),
  ('if_stmt_part3 -> block','if_stmt_part3',1,'p_if_stmt_part3','parser.py',398),
  ('switch_stmt -> expr_switch_stmt','switch_stmt',1,'p_switch_stmt','parser.py',402),
  ('expr_switch_stmt -> SWITCH expr_switch_stmt_part1 LBRACE expr_switch_stmt_part2 RBRACE','expr_switch_stmt',5,'p_expr_switch_stmt','parser.py',406),
  ('expr_switch_stmt_part1 -> expression','expr_switch_stmt_part1',1,'p_expr_switch_stmt_part1','parser.py',412),
  ('expr_switch_stmt_part1 -> empty','expr_switch_stmt_part1',1,'p_expr_switch_stmt_part1','parser.py',413),
  ('expr_switch_stmt_part2 -> expr_case_clause expr_switch_stmt_part2','expr_switch_stmt_part2',2,'p_expr_switch_stmt_part2','parser.py',417),
  ('expr_switch_stmt_part2 -> empty','expr_switch_stmt_part2',1,'p_expr_switch_stmt_part2','parser.py',418),
  ('expr_case_clause -> expr_switch_case COLON statement_list','expr_case_clause',3,'p_expr_case_clause','parser.py',422),
  ('expr_switch_case -> CASE expression_list','expr_switch_case',2,'p_expr_switch_case','parser.py',426),
  ('expr_switch_case -> DEFAULT','expr_switch_case',1,'p_expr_switch_case','parser.py',427),
  ('for_stmt -> FOR for_stmt_part block','for_stmt',3,'p_for_stmt','parser.py',431),
  ('for_stmt_part -> condition','for_stmt_part',1,'p_for_stmt_part','parser.py',435),
  ('for_stmt_part -> for_clause','for_stmt_part',1,'p_for_stmt_part','parser.py',436),
  ('for_clause -> init_stmt SEMICOL condition SEMICOL post_stmt','for_clause',5,'p_for_clause','parser.py',441),
  ('init_stmt -> simple_stmt','init_stmt',1,'p_init_stmt','parser.py',445),
  ('post_stmt -> simple_stmt','post_stmt',1,'p_post_stmt','parser.py',449),
  ('condition -> expression','condition',1,'p_condition','parser.py',452),
  ('condition -> empty','condition',1,'p_condition','parser.py',453),
  ('go_stmt -> GO expression','go_stmt',2,'p_go_stmt','parser.py',467),
  ('go_to_stmt -> GOTO IDENTIFIER','go_to_stmt',2,'p_go_to_stmt','parser.py',470),
  ('return_stmt -> RETURN expr_switch_stmt_part1','return_stmt',2,'p_return_stmt','parser.py',474),
  ('break_stmt -> BREAK break_stmt_part1','break_stmt',2,'p_break_stmt','parser.py',477),
  ('break_stmt_part1 -> label','break_stmt_part1',1,'p_break_stmt_part1','parser.py',481),
  ('break_stmt_part1 -> empty','break_stmt_part1',1,'p_break_stmt_part1','parser.py',482),
  ('continue_stmt -> CONTINUE break_stmt_part1','continue_stmt',2,'p_continue_stmt','parser.py',486),
]
